package com.zc.utilclass.arrayutil.main;

/**
 * 
 * @author zhangchao 定义自己的一维数组类
 */
public class ArrayUtil2 {
	// 定义一个初始长度为0的数组，用来缓存数据
	private long[] arrlong; //= new long[0];
	private int[] arrint;// = new int[0];
	private short[] arrshort;// = new short[0];
	private char[] arrchar;// = new char[0];
	private byte[] arrbyte;// = new byte[0];
	private boolean[] arrbool;// = new boolean[0];
	private float[] arrfloat;// = new float[0];
	private double[] arrdouble;// = new double[0];
	private Object[] arrobj;// = new Object[0];
	
	public ArrayUtil2() {
		arrlong = new long[0];
		arrint = new int[0];
		arrshort = new short[0];
		arrchar = new char[0];
		arrbyte = new byte[0];
		arrbool = new boolean[0];
		arrfloat = new float[0];
		arrdouble = new double[0];
		arrobj = new Object[0];
	}
	
	public ArrayUtil2(long[] arrlong) {
		this.setArrlong(arrlong);
	}
	
	public ArrayUtil2(int[] arrint) {
		this.setArrint(arrint);
	}
	
	public ArrayUtil2(short[] arrshort) {
		this.setArrshort(arrshort);
	}
	
	public ArrayUtil2(char[] arrchar) {
		this.setArrchar(arrchar);
	}
	
	public ArrayUtil2(byte[] arrbyte) {
		this.setArrbyte(arrbyte);
	}
	
	public ArrayUtil2(boolean[] arrbool) {
		this.setArrbool(arrbool);
	}
	
	public ArrayUtil2(float[] arrfloat) {
		this.setArrfloat(arrfloat);
	}
	
	public ArrayUtil2(double[] arrdouble) {
		this.setArrfloat(arrfloat);
	}
	
	public ArrayUtil2(Object[] arrobj) {
		this.setArrobj(arrobj);
	}

	public long[] getArrlong() {
		return arrlong;
	}

	public void setArrlong(long[] arrlong) {
		this.arrlong = arrlong;
	}

	public int[] getArrint() {
		return arrint;
	}

	public void setArrint(int[] arrint) {
		this.arrint = arrint;
	}

	public short[] getArrshort() {
		return arrshort;
	}

	public void setArrshort(short[] arrshort) {
		this.arrshort = arrshort;
	}

	public char[] getArrchar() {
		return arrchar;
	}

	public void setArrchar(char[] arrchar) {
		this.arrchar = arrchar;
	}

	public byte[] getArrbyte() {
		return arrbyte;
	}

	public void setArrbyte(byte[] arrbyte) {
		this.arrbyte = arrbyte;
	}

	public boolean[] getArrbool() {
		return arrbool;
	}

	public void setArrbool(boolean[] arrbool) {
		this.arrbool = arrbool;
	}

	public float[] getArrfloat() {
		return arrfloat;
	}

	public void setArrfloat(float[] arrfloat) {
		this.arrfloat = arrfloat;
	}

	public double[] getArrdouble() {
		return arrdouble;
	}

	public void setArrdouble(double[] arrdouble) {
		this.arrdouble = arrdouble;
	}

	public Object[] getArrobj() {
		return arrobj;
	}

	public void setArrobj(Object[] arrobj) {
		this.arrobj = arrobj;
	}

	// 数组长度加1，并在末尾增加元素
	public void add(long s) {
		// 定义新数组，长度是原数组长度+1
		long[] dest = new long[arrlong.length + 1];
		// 将原数组的数据拷贝到新数组
		System.arraycopy(arrlong, 0, dest, 0, arrlong.length);
		// 将新元素放到dest数组的末尾
		dest[arrlong.length] = s;
		// 将arrlong指向dest
		arrlong = dest;
	}

	// 增加
	public void add(int s) {
		// 定义新数组，长度是原数组长度+1
		int[] dest = new int[arrint.length + 1];
		// 将原数组的数据拷贝到新数组
		System.arraycopy(arrint, 0, dest, 0, arrint.length);
		// 将新元素放到dest数组的末尾
		dest[arrint.length] = s;
		// 将arrint指向dest
		arrint = dest;
	}

	// 增加
	public void add(short s) {
		// 定义新数组，长度是原数组长度+1
		short[] dest = new short[arrshort.length + 1];
		// 将原数组的数据拷贝到新数组
		System.arraycopy(arrshort, 0, dest, 0, arrshort.length);
		// 将新元素放到dest数组的末尾
		dest[arrshort.length] = s;
		// 将arrshort指向dest
		arrshort = dest;
	}

	// 增加
	public void add(char s) {
		// 定义新数组，长度是原数组长度+1
		char[] dest = new char[arrchar.length + 1];
		// 将原数组的数据拷贝到新数组
		System.arraycopy(arrchar, 0, dest, 0, arrchar.length);
		// 将新元素放到dest数组的末尾
		dest[arrchar.length] = s;
		// 将arrchar指向dest
		arrchar = dest;
	}

	// 增加
	public void add(byte s) {
		// 定义新数组，长度是原数组长度+1
		byte[] dest = new byte[arrbyte.length + 1];
		// 将原数组的数据拷贝到新数组
		System.arraycopy(arrbyte, 0, dest, 0, arrbyte.length);
		// 将新元素放到dest数组的末尾
		dest[arrbyte.length] = s;
		// 将arrbyte指向dest
		arrbyte = dest;
	}

	// 增加
	public void add(boolean s) {
		// 定义新数组，长度是原数组长度+1
		boolean[] dest = new boolean[arrbool.length + 1];
		// 将原数组的数据拷贝到新数组
		System.arraycopy(arrbool, 0, dest, 0, arrbool.length);
		// 将新元素放到dest数组的末尾
		dest[arrbool.length] = s;
		// 将arrbool指向dest
		arrbool = dest;
	}

	// 增加
	public void add(float s) {
		// 定义新数组，长度是原数组长度+1
		float[] dest = new float[arrfloat.length + 1];
		// 将原数组的数据拷贝到新数组
		System.arraycopy(arrfloat, 0, dest, 0, arrfloat.length);
		// 将新元素放到dest数组的末尾
		dest[arrfloat.length] = s;
		// 将arrfloat指向dest
		arrfloat = dest;
	}

	// 增加
	public void add(double s) {
		// 定义新数组，长度是原数组长度+1
		double[] dest = new double[arrdouble.length + 1];
		// 将原数组的数据拷贝到新数组
		System.arraycopy(arrdouble, 0, dest, 0, arrdouble.length);
		// 将新元素放到dest数组的末尾
		dest[arrdouble.length] = s;
		// 将arrdouble指向dest
		arrdouble = dest;
	}

	// 增加
	public void add(Object s) {
		// 定义新数组，长度是原数组长度+1
		Object[] dest = new Object[arrobj.length + 1];
		// 将原数组的数据拷贝到新数组
		System.arraycopy(arrobj, 0, dest, 0, arrobj.length);
		// 将新元素放到dest数组的末尾
		dest[arrobj.length] = s;
		// 将arrobj指向dest
		arrobj = dest;
	}

	// 修改指定位置的元素
	public void modify(int index, long s) {
		arrlong[index] = s;
	}

	// 修改指定位置的元素
	public void modify(int index, int s) {
		arrint[index] = s;
	}

	// 修改指定位置的元素
	public void modify(int index, short s) {
		arrshort[index] = s;
	}

	// 修改指定位置的元素
	public void modify(int index, char s) {
		arrchar[index] = s;
	}

	// 修改指定位置的元素
	public void modify(int index, byte s) {
		arrbyte[index] = s;
	}

	// 修改指定位置的元素
	public void modify(int index, boolean s) {
		arrbool[index] = s;
	}
	
	// 修改指定位置的元素
	public void modify(int index, float s) {
		arrfloat[index] = s;
	}

	// 修改指定位置的元素
	public void modify(int index, double s) {
		arrdouble[index] = s;
	}

	// 修改指定位置的元素
	public void modify(int index, Object s) {
		arrobj[index] = s;
	}

	// 删除指定位置的元素
	public void delete(int index, long s) {
		long[] destlong = new long[arrlong.length - 1];
		// 将原数组的数据拷贝到新数组
		System.arraycopy(arrlong, 0, destlong, 0, index);
		System.arraycopy(arrlong, index + 1, destlong, index, arrlong.length - 1 - index);
		arrlong = destlong;
	}
	
	// 删除指定位置的元素
	public void delete(int index, int s) {
		int[] destint = new int[arrint.length - 1];
		// 将原数组的数据拷贝到新数组
		System.arraycopy(arrint, 0, destint, 0, index);
		System.arraycopy(arrint, index + 1, destint, index, arrint.length - 1 - index);
		arrint = destint;
	}
	
	// 删除指定位置的元素
	public void delete(int index, short s) {
		short[] destshort = new short[arrshort.length - 1];
		// 将原数组的数据拷贝到新数组
		System.arraycopy(arrshort, 0, destshort, 0, index);
		System.arraycopy(arrshort, index + 1, destshort, index, arrshort.length - 1 - index);
		arrshort = destshort;
	}
	
	// 删除指定位置的元素
	public void delete(int index, char s) {
		char[] destchar = new char[arrchar.length - 1];
		// 将原数组的数据拷贝到新数组
		System.arraycopy(arrchar, 0, destchar, 0, index);
		System.arraycopy(arrchar, index + 1, destchar, index, arrchar.length - 1 - index);
		arrchar = destchar;
	}
	
	// 删除指定位置的元素
	public void delete(int index, byte s) {
		byte[] destbyte = new byte[arrbyte.length - 1];
		// 将原数组的数据拷贝到新数组
		System.arraycopy(arrbyte, 0, destbyte, 0, index);
		System.arraycopy(arrbyte, index + 1, destbyte, index, arrbyte.length - 1 - index);
		arrbyte = destbyte;
	}
	
	// 删除指定位置的元素
	public void delete(int index, boolean s) {
		boolean[] destbool = new boolean[arrbool.length - 1];
		// 将原数组的数据拷贝到新数组
		System.arraycopy(arrbool, 0, destbool, 0, index);
		System.arraycopy(arrbool, index + 1, destbool, index, arrbool.length - 1 - index);
		arrbool = destbool;
	}
	
	// 删除指定位置的元素
	public void delete(int index, float s) {
		float[] destfloat = new float[arrfloat.length - 1];
		// 将原数组的数据拷贝到新数组
		System.arraycopy(arrfloat, 0, destfloat, 0, index);
		System.arraycopy(arrfloat, index + 1, destfloat, index, arrfloat.length - 1 - index);
		arrfloat = destfloat;
	}
	
	// 删除指定位置的元素
	public void delete(int index, double s) {
		double[] destdouble = new double[arrdouble.length - 1];
		// 将原数组的数据拷贝到新数组
		System.arraycopy(arrdouble, 0, destdouble, 0, index);
		System.arraycopy(arrdouble, index + 1, destdouble, index, arrdouble.length - 1 - index);
		arrdouble = destdouble;
	}
	
	// 删除指定位置的元素
	public void delete(int index, Object s) {
		Object[] dest = new Object[arrobj.length - 1];
		// 将原数组的数据拷贝到新数组
		System.arraycopy(arrobj, 0, dest, 0, index);
		System.arraycopy(arrobj, index + 1, dest, index, arrobj.length - 1 - index);
		arrobj = dest;
	}

	// 获得指定位置的元素
	public Object get(int index) {
		return arrobj[index];
	}

	// 在指定位置插入指定元素
	public void insert(int index, Object s) {
		// 定义新数组，长度是原数组长度+1
		Object[] dest = new Object[arrobj.length + 1];
		// 将原数组的数据拷贝到新数组
		System.arraycopy(arrobj, 0, dest, 0, index);
		dest[index] = s;
		System.arraycopy(arrobj, index, dest, index + 1, arrobj.length - index);
		arrobj = dest;

	}

	// 获得元素个数
	public int size() {
		return arrobj.length;
	}

	public void print() {
		for (int i = 0; i < size(); i++)
			System.out.println(arrobj[i]);
	}
}
