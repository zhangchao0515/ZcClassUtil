package com.zc.utilclass.arrayutil.main;

/**
 * 
 * @author zhangchao 定义自己的一维数组类
 */
public class ArrayUtil {
	// 定义一个初始长度为0的数组，用来缓存数据
	private Object[] arrobj;// = new Object[0];
	
	public ArrayUtil() {
		arrobj = new Object[0];
	}
	
	public ArrayUtil(Object[] arrobj) {
		this.setArrobj(arrobj);
	}

	public Object[] getArrobj() {
		return arrobj;
	}

	public void setArrobj(Object[] arrobj) {
		this.arrobj = arrobj;
	}

	// 增加
	public void add(Object s) {
		// 定义新数组，长度是原数组长度+1
		Object[] dest = new Object[arrobj.length + 1];
		// 将原数组的数据拷贝到新数组
		System.arraycopy(arrobj, 0, dest, 0, arrobj.length);
		// 将新元素放到dest数组的末尾
		dest[arrobj.length] = s;
		// 将arrobj指向dest
		arrobj = dest;
	}

	// 修改指定位置的元素
	public void modify(int index, Object s) {
		arrobj[index] = s;
	}
	
	// 删除指定位置的元素
	public void delete(int index) {
		Object[] dest = new Object[arrobj.length - 1];
		// 将原数组的数据拷贝到新数组
		System.arraycopy(arrobj, 0, dest, 0, index);
		System.arraycopy(arrobj, index + 1, dest, index, arrobj.length - 1 - index);
		arrobj = dest;
	}

	// 获得指定位置的元素
	public Object get(int index) {
		return arrobj[index];
	}

	// 在指定位置插入指定元素
	public void insert(int index, Object s) {
		// 定义新数组，长度是原数组长度+1
		Object[] dest = new Object[arrobj.length + 1];
		// 将原数组的数据拷贝到新数组
		System.arraycopy(arrobj, 0, dest, 0, index);
		dest[index] = s;
		System.arraycopy(arrobj, index, dest, index + 1, arrobj.length - index);
		arrobj = dest;

	}

	// 获得元素个数
	public int size() {
		return arrobj.length;
	}

	public void print() {
		int count = 0;
		for (int i = 0; i < size(); i++) {
			System.out.print(arrobj[i] + " ");
			count++;
			if(count%10==0) {
				System.out.println();
			}
		}
		System.out.println();	
	}
}
