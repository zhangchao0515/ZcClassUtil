package com.zc.utilclass.dateutil.main;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SimpleDateDemo {

	public static void main(String[] args) {
		Date nowDate = new Date();
		String formatDate = DateFormat.getDateInstance().format(nowDate);  
		System.out.println(formatDate);
		formatDate = DateFormat.getDateInstance(DateFormat.FULL).format(nowDate);
		System.out.println(formatDate);
		formatDate = DateFormat.getDateInstance(0).format(nowDate);
		System.out.println(formatDate);
		formatDate = DateFormat.getDateInstance(1).format(nowDate);
		System.out.println(formatDate);
		formatDate = DateFormat.getDateInstance(2).format(nowDate);
		System.out.println(formatDate);
		formatDate = DateFormat.getDateInstance(3).format(nowDate);
		System.out.println(formatDate);
		formatDate = new SimpleDateFormat("yyyy-MM-dd").format(nowDate);
		System.out.println(formatDate);
		formatDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(nowDate);
		System.out.println(formatDate);
		formatDate = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(nowDate);
		System.out.println(formatDate);
		formatDate = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒").format(nowDate);
		System.out.println(formatDate);
		formatDate = new SimpleDateFormat("HH:mm:ss").format(nowDate);
		System.out.println(formatDate);
		System.out.println();
	}

}
