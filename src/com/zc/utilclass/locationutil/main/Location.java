package com.zc.utilclass.locationutil.main;

/**
 * 经纬度
 * @author zhangchao
 *
 */
public class Location {
	private double lng; //经度
	private double lat; //纬度
	//location[0]表示经度,location[1]表示纬度
	//private double location[] = new double[2];
	
	/**
	 * 获取经纬度
	 */
	public double[] getLocation() {
		return new double[] {lng,lat};
	}
	//获取经度
	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}
	//获取纬度
	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	/**
	 * 获取经纬度
	 */
	public String[] getLocationString() {
		String locationStr[] = new String[2];
		locationStr[0] = String.valueOf(lng);
		locationStr[1] = String.valueOf(lat);
		return locationStr;
	}
	
	/**
	 * 设置经纬度
	 * @param coordinate
	 */
	public void setLocation(double[] location) {
		this.lng = location[0];
		this.lat = location[1];
	}
	/**
	 * 设置经纬度
	 * @param lng 经度
	 * @param lat 纬度
	 */
	public void setLocation(double lng,double lat) {
		this.lng = lng;
		this.lat = lat;
	}
	
	public Location() {
		this.setLocation(0,0);
	}
	public Location(double[] location) {
		this.setLocation(location);
	}
	public Location(double lng,double lat) {
		this.setLocation(lng, lat);
	}
	
	@Override
	public String toString() {
		return "location:{"+"lng:"+this.lng + ","+ "lat:"+this.lat +"]";
	}
	
	public String toJson() {
		return "[\""+this.lng + "\",\""+ this.lat +"\"]";
	}
}
