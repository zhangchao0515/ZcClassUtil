package com.zc.utilclass.locationutil.main;

/**
 * 中国相关信息
 * @author zhangchao
 *
 */
public class China {
	final String chineseName = "中国";
	final String englishName = "China";
	
	//ChineseProvince cp = new ChineseProvince();
	
	// ...其他信息
	// 56个民族
	final String nationality[] = {};
	
	public China() {}

	public String getChineseName() {
		return chineseName;
	}

	public String getEnglishName() {
		return englishName;
	}

	public String[] getNationality() {
		return nationality;
	}
	
	/**
	 * 判断是否是中国省份
	 * @param province 省份
	 * @return 
	 */
	public boolean isChinsesProvince(String province) {
		
		return false;
	}
}
