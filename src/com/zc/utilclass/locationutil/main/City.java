package com.zc.utilclass.locationutil.main;

public class City {
	int code; //该城市的代码编号（行政代码）
	String chineseName; //城市中文名称
	String englishName; //城市英文名称
	String[] alias; //别名
	String[] category; //该城市的类别（行政区类别）
	double[] coordinate; //该城市的经纬度
	String affiArea; //该城市所属地区 affiliatedArea
	String[] areaUnderJuri; //该城市下辖地区(不同类型分类计数)
	String[] areaUnderJuriDetail; //该城市下辖的所有地区
	String teleCodes; //电话区号
	String zipCode; //邮政区码
	String plateNumber; //车牌代码
	String[] airport; //机场
	String[] trainStation; //火车站
	String address; //地理位置
	String area; //面积
	String population; //人口
	String gdp; //
	String dialect; //方言
	String climate; //气候条件
	String[] famousScenicSpot; //著名景点
	String goverStation; //政府驻地
	String[] highEdu; //高等学府
	
	public City() {
		//this(0,"","",null,null,null,"",null,null,"","","",null,null,"","","","","","",null,"",null);
		this(0,"","",null,null,null,"",null,null,"","","");
	}
	
	public City(int code,String chineseName,String englishName,String[] alias,String[] category,double[] coordinate,
			String affiArea,String[] areaUnderJuri,String[] areaUnderJuriDetail,String teleCodes,
			String zipCode,String plateNumber) {
		this(code,chineseName,englishName,alias,category,coordinate,affiArea,
				areaUnderJuri,areaUnderJuriDetail,teleCodes,zipCode,plateNumber,
				null,null,"","","","","","",null,"",null);
	}
	
	public City(int code,String chineseName,String englishName,String[] alias,String[] category,double[] coordinate,
			String affiArea,String[] areaUnderJuri,String[] areaUnderJuriDetail,String teleCodes,
			String zipCode,String plateNumber,String[] airport,String[] trainStation,String address,
			String area,String population,String gdp,String dialect,String climate,String[] famousScenicSpot,
			String goverStation,String[] highEdu) {
		this.setCode(code);
		this.setChineseName(chineseName);
		this.setEnglishName(englishName);
		this.setAlias(alias);
		this.setCategory(category);
		this.setCoordinate(coordinate);
		this.setAffiArea(affiArea);
		this.setAreaUnderJuri(areaUnderJuriDetail);
		this.setAreaUnderJuriDetail(areaUnderJuriDetail);
		this.setTeleCodes(teleCodes);
		this.setZipCode(zipCode);
		this.setPlateNumber(plateNumber);
		this.setAirport(airport);
		this.setTrainStation(trainStation);
		this.setAddress(address);
		this.setArea(area);
		this.setPopulation(population);
		this.setGdp(gdp);
		this.setDialect(dialect);
		this.setClimate(climate);
		this.setFamousScenicSpot(famousScenicSpot);
		this.setGoverStation(goverStation);
		this.setHighEdu(highEdu);
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getChineseName() {
		return chineseName;
	}

	public void setChineseName(String chineseName) {
		this.chineseName = chineseName;
	}

	public String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

	public String[] getAlias() {
		return alias;
	}

	public void setAlias(String[] alias) {
		this.alias = alias;
	}

	public String[] getCategory() {
		return category;
	}

	public void setCategory(String[] category) {
		this.category = category;
	}

	public double[] getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(double[] coordinate) {
		this.coordinate = coordinate;
	}

	public String getAffiArea() {
		return affiArea;
	}

	public void setAffiArea(String affiArea) {
		this.affiArea = affiArea;
	}

	public String[] getAreaUnderJuri() {
		return areaUnderJuri;
	}

	public void setAreaUnderJuri(String[] areaUnderJuri) {
		this.areaUnderJuri = areaUnderJuri;
	}

	public String[] getAreaUnderJuriDetail() {
		return areaUnderJuriDetail;
	}

	public void setAreaUnderJuriDetail(String[] areaUnderJuriDetail) {
		this.areaUnderJuriDetail = areaUnderJuriDetail;
	}

	public String getTeleCodes() {
		return teleCodes;
	}

	public void setTeleCodes(String teleCodes) {
		this.teleCodes = teleCodes;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getPlateNumber() {
		return plateNumber;
	}

	public void setPlateNumber(String plateNumber) {
		this.plateNumber = plateNumber;
	}

	public String[] getAirport() {
		return airport;
	}

	public void setAirport(String[] airport) {
		this.airport = airport;
	}

	public String[] getTrainStation() {
		return trainStation;
	}

	public void setTrainStation(String[] trainStation) {
		this.trainStation = trainStation;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getPopulation() {
		return population;
	}

	public void setPopulation(String population) {
		this.population = population;
	}

	public String getGdp() {
		return gdp;
	}

	public void setGdp(String gdp) {
		this.gdp = gdp;
	}

	public String getDialect() {
		return dialect;
	}

	public void setDialect(String dialect) {
		this.dialect = dialect;
	}

	public String getClimate() {
		return climate;
	}

	public void setClimate(String climate) {
		this.climate = climate;
	}

	public String[] getFamousScenicSpot() {
		return famousScenicSpot;
	}

	public void setFamousScenicSpot(String[] famousScenicSpot) {
		this.famousScenicSpot = famousScenicSpot;
	}

	public String getGoverStation() {
		return goverStation;
	}

	public void setGoverStation(String goverStation) {
		this.goverStation = goverStation;
	}

	public String[] getHighEdu() {
		return highEdu;
	}

	public void setHighEdu(String[] highEdu) {
		this.highEdu = highEdu;
	}
	
}
