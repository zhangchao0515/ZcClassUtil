package com.zc.utilclass.locationutil.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/*
 * @date 2018-08-08 16:54
 * @author zhangchao
 * 获取城市的经纬度, 需要联网连接百度地图查询
 */
public class LocationUtil {
	private String city; // 城市
	//private String lng; //经度
	//private String lat; //纬度
	
	public LocationUtil() {
		
	}
	
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	/** 
  	* 根据城市名称查询所在经纬度,需要联网连接百度地图查询
  	* @author zhangchao
  	* @date 2018-08-08
    * @param addr 查询的地址 
    * @return 
    * @throws IOException 
    */
	public String[] getCoordinate(String addr) throws IOException {
		String lng = null;//经度
        String lat = null;//纬度
        String address = null; 
        addr = province2City(addr);
        try { 
            address = java.net.URLEncoder.encode(addr, "UTF-8"); 
        }catch (UnsupportedEncodingException e1) { 
            e1.printStackTrace(); 
        } 
        String key = "NcMnc56RX48MjpsOfP4ZEW5GVHmCCmeg"; 
        String url = String .format("http://api.map.baidu.com/geocoder?address=%s&output=json&key=%s", address, key); 
        URL myURL = null; 
        URLConnection httpsConn = null; 
        try { 
            myURL = new URL(url); 
        } catch (MalformedURLException e) { 
            e.printStackTrace(); 
        } 
        InputStreamReader insr = null;
        BufferedReader br = null;
        try { 
            httpsConn = (URLConnection) myURL.openConnection();// 不使用代理 
            if (httpsConn != null) { 
                insr = new InputStreamReader( httpsConn.getInputStream(), "UTF-8"); 
                br = new BufferedReader(insr); 
                String data = null; 
                int count = 1;
                while((data= br.readLine())!=null){ 
                    if(count==5){
                        lng = (String)data.subSequence(data.indexOf(":")+1, data.indexOf(","));//经度
                        count++;
                    }else if(count==6){
                        lat = data.substring(data.indexOf(":")+1);//纬度
                        count++;
                    }else{
                        count++;
                    }
                } 
            } 
        } catch (IOException e) { 
            e.printStackTrace(); 
        } finally {
            if(insr!=null){
                insr.close();
            }
            if(br!=null){
                br.close();
            }
        }

        return new String[]{lng,lat}; 
	}
	
	/** 
  	* 将省转化成省会城市
  	* @author zhangchao
  	* @date 2018-08-08 16:58
    * @param addr 查询的地址 
    * @return 
    * @throws IOException 
    */
	public String province2City(String province) {
		//--哈尔滨
		switch(province) {
			case "黑龙江":
			case "黑龙江省":
				province = "哈尔滨";
				break;
			case "吉林":
			case "吉林省":
				province = "长春";
				break;
			case "辽宁":
			case "辽宁省":
				province = "沈阳";
				break;
			case "内蒙古":
			case "内蒙古自治区":
				province = "呼和浩特";
				break;
			case "河北":
			case "河北省":
				province = "石家庄";
				break;
			case "新疆":
			case "新疆维吾尔自治区":
				province = "乌鲁木齐";
				break;
			case "甘肃":
			case "甘肃省":
				province = "兰州";
				break;
			case "青海":
			case "青海省":
				province = "西宁";
				break;
			case "陕西":
			case "陕西省":
				province = "西安";
				break;
			case "宁夏":
			case "宁夏回族自治区":
				province = "银川";
				break;
			case "河南":
			case "河南省":
				province = "郑州";
				break;
			case "山东":
			case "山东省":
				province = "济南";
				break;
			case "山西":
			case "山西省":
				province = "太原";
				break;
			case "安徽":
			case "安徽省":
				province = "合肥";
				break;
			case "湖南":
			case "湖南省":
				province = "长沙";
				break;
			case "湖北":
			case "湖北省":
				province = "武汉";
				break;
			case "江苏":
			case "江苏省":
				province = "南京";
				break;
			case "四川":
			case "四川省":
				province = "成都";
				break;
			case "贵州":
			case "贵州省":
				province = "贵阳";
				break;
			case "云南":
			case "云南省":
				province = "昆明";
				break;
			case "广西":
			case "广西壮族自治区":
				province = "南宁";
				break;
			case "西藏":
			case "西藏自治区":
				province = "拉萨";
				break;
			case "浙江":
			case "浙江省":
				province = "杭州";
				break;
			case "江西":
			case "江西省":
				province = "南昌";
				break;
			case "广东":
			case "广东省":
				province = "广州";
				break;
			case "福建":
			case "福建省":
				province = "福州";
				break;
			case "台湾":
			case "台湾省":
				province = "台北";
				break;
			case "海南":
			case "海南省":
				province = "海口";
				break;
			case "香港特别行政区":
				province = "香港";
				break;
			case "澳门特别行政区":
				province = "澳门";
				break;
			default:
				break;
		}

		return province;
	}
}
