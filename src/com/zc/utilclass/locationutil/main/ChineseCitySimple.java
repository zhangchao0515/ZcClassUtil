package com.zc.utilclass.locationutil.main;

import java.util.HashMap;
import java.util.Map;

/**
 * 中国的市((地级)市、自治州、地区、林区、盟)
 * 自治州、地区、林区、盟用全称
 * @author zhangchao 
 * @date 2018-09-12
 * 省份指中国的行政规划，我国共有34个省级行政区域，包括23个省，5个自治区，4个直辖市，2个特别行政区。
 */
public class ChineseCitySimple {
	// 城市经纬度
	Map<String, double[]> cityLoc = new HashMap<String,double[]>();
	// 4个直辖市
	final String municipality[] = { "北京市", "天津市", "上海市", "重庆市" };
	// 2个特别行政区
	final String specialAdminRegion[] = { "香港特别行政区", "澳门特别行政区" };
	
	//包括23个省，5个自治区 又包含多个市
	// 第一个是该省的省会城市
	/**
	 * 河北省
	 * 河北省有11个地级市，两个省直管县级市，并下辖国家级雄安新区。
	 * 11个地级市分别为：石家庄市，唐山市，秦皇岛市，邯郸市，邢台市，保定市，张家口市，承德市，沧州市，廊坊市，衡水市。
	 * 两个省直管县级市为：定州市和辛集市。
	 */
	final String heBei[] = {"石家庄市","唐山市","秦皇岛市","邯郸市","邢台市","保定市","张家口市","承德市","沧州市","廊坊市","衡水市"};
	/**
	 * 山西省
	 * 山西省11个地级市：太原市、大同市、朔州市、阳泉市、长治市、忻州市、吕梁市、晋中市、临汾市、运城市、晋城市
	 */
	final String shanXi[] = {"太原市","大同市","朔州市","阳泉市","长治市","忻州市","吕梁市","晋中市","临汾市","运城市","晋城市"};
	/**
	 * 辽宁省
	 * 辽宁省14个地级市：沈阳市、大连市、鞍山市、抚顺市、本溪市、丹东市、锦州市、营口市、阜新市、辽阳市、盘锦市、铁岭市、朝阳市、葫芦岛市
	 */
	final String liaoNing[] = {"沈阳市","大连市","鞍山市","抚顺市","本溪市","丹东市","锦州市","营口市","阜新市","辽阳市","盘锦市","铁岭市","朝阳市","葫芦岛市"};
	/**
	 * 吉林省
	 * 辖8个地级市、1个自治州（合计9个地级行政区划单位）
	 * 吉林省一共有8个地级市：长春市、吉林市、四平市、辽源市、通化市、白山市、松原市、白城市
	 * 1个自治州：延边朝鲜族自治州
	 */
	final String jiLin[] = {"长春市","吉林市","四平市","辽源市","通化市","白山市","松原市","白城市","延边朝鲜族自治州"};
	/**
	 * 黑龙江省
	 * 黑龙江共有12个地级市1个地区。
	 * 12个地级市：分别是哈尔滨，齐齐哈尔，牡丹江，佳木斯，大庆，鸡西，双鸭山，鹤岗，伊春，七台河，黑河，绥化
	 * 1个地区：大兴安岭地区
	 */
	final String heiLongJiang[] = {"哈尔滨市","齐齐哈尔市","牡丹江市","佳木斯市","大庆市","鸡西市","双鸭山市","鹤岗市","伊春市","七台河市","黑河市","绥化市","大兴安岭地区"};
	/**
	 * 江苏省
	 * 江苏省共有13个地级市。
	 * 13个地级市：分别是南京市、徐州市、常州市、淮安市、南通市、宿迁市、无锡市、扬州市、盐城市、镇江市、泰州市、苏州市、连云港市
	 */
	final String jiangSu[] = {"南京市","徐州市","常州市","淮安市","南通市","宿迁市","无锡市","扬州市","盐城市","镇江市","泰州市","苏州市","连云港市"};
	/**
	 * 浙江省
	 * 浙江省共有11个地级市。
	 * 11个地级市：分别是杭州市、宁波市、温州市、绍兴市、湖州市、嘉兴市、金华市、衢州市、台州市、丽水市、舟山市
	 */
	final String zheJiang[] = {"杭州市","宁波市","温州市","绍兴市","湖州市","嘉兴市","金华市","衢州市","台州市","丽水市","舟山市"};
	/**
	 * 安徽省
	 * 安徽省共有17个地级市。
	 * 17个地级市：分别是合肥市、蚌埠市、芜湖市、淮南市、马鞍山市、淮北市、铜陵市、安庆市、黄山市、阜阳市、宿州市、滁州市、六安市、宣城市、巢湖市、池州市、亳州市
	 */
	final String anHui[] = {"合肥市","蚌埠市","芜湖市","淮南市","马鞍山市","淮北市","铜陵市","安庆市","黄山市","阜阳市","宿州市","滁州市","六安市","宣城市","巢湖市","池州市","亳州市"};
	/**
	 * 福建省
	 * 福建省共有9地级市。
	 * 9地级市：分别是福州市、龙岩市、南平市、宁德市、莆田市、泉州市、三明市、厦门市、漳州市。
	 */
	final String fuJian[] = {"福州市","龙岩市","南平市","宁德市","莆田市","泉州市","三明市","厦门市","漳州市"};
	/**
	 * 江西省
	 * 江西省共有11地级市。
	 * 11地级市：分别是南昌市、赣州市、吉安市、萍乡市、宜春市、新余市、景德镇市、抚州市、九江市、上饶市、鹰潭市。
	 */
	final String jiangXi[] = {"南昌市","赣州市","吉安市","萍乡市","宜春市","新余市","景德镇市","抚州市","九江市","上饶市","鹰潭市"};
	/**
	 * 山东省
	 * 山东省共有17地级市。
	 * 17地级市：分别是济南市、青岛市、烟台市、威海市、东营市、淄博市、潍坊市、日照市、莱芜市、菏泽市、枣庄市、德州市、滨州市、临沂市、济宁市、聊城市、泰安市
	 */
	final String shanDong[] = {"济南市","青岛市","烟台市","威海市","东营市","淄博市","潍坊市","日照市","莱芜市","菏泽市","枣庄市","德州市","滨州市","临沂市","济宁市","聊城市","泰安市"};
	/**
	 * 河南省
	 * 河南省共有17个地级市、1个省直辖县级行政单位
	 * 17地级市：分别是郑州市、开封市、洛阳市、平顶山市、安阳市、鹤壁市、新乡市、焦作市、濮阳市、许昌市、漯河市、三门峡市、南阳市、商丘市、信阳市、周口市、驻马店市
	 * 1个省直辖县级行政单位：济源市
	 */
	final String heNan[] = {"郑州市","开封市","洛阳市","平顶山市","安阳市","鹤壁市","新乡市","焦作市","濮阳市","许昌市","漯河市","三门峡市","南阳市","商丘市","信阳市","周口市","驻马店市","济源市"};
	/**
	 * 湖北省
	 * 湖北省共有17个地级市、1个省直辖县级行政单位
	 * 湖北省现下辖：1个副省级市（武汉市）；
	 * 11个地级市（黄石市、十堰市、荆州市、宜昌市、襄阳市、鄂州市、荆门市、黄冈市、孝感市、咸宁市、随州市）；
	 * 1个自治州（恩施土家族苗族自治州）；
	 * 3个直管市（仙桃市、天门市、潜江市）；
	 * 1个林区（神农架林区）。
	 */
	final String huBei[] = {"武汉市","黄石市","十堰市","荆州市","宜昌市","襄阳市","鄂州市","荆门市","黄冈市","孝感市","咸宁市","随州市","恩施土家族苗族自治州","仙桃市","天门市","潜江市","神农架林区"};
	/**
	 * 湖南省
	 * 湖南省共有13个地级市、1个自治州
	 * 13个地级市：分别是长沙市, 株洲市, 湘潭市, 衡阳市, 永州市, 邵阳市, 岳阳市, 张家界市, 常德市, 益阳市, 娄底市, 郴州市, 怀化市
	 * 1个自治州： 湘西土家族苗族自治州
	 */
	final String huNan[] = {"长沙市","株洲市","湘潭市","衡阳市","永州市","邵阳市","岳阳市","张家界市","常德市","益阳市","娄底市","郴州市","怀化市","湘西土家族苗族自治州"};
	/**
	 * 广东省
	 * 广东省共有21个地级市
	 * 21个地级市：分别是广州市、深圳市、珠海市、汕头市、佛山市、韶关市、湛江市、肇庆市、江门市、茂名市、惠州市、梅州市、汕尾市、河源市、阳江市、清远市、东莞市、中山市、潮州市、揭阳市、云浮市
	 */
	final String guangDong[] = {"广州市","深圳市","珠海市","汕头市","佛山市","韶关市","湛江市","肇庆市","江门市","茂名市","惠州市","梅州市","汕尾市","河源市","阳江市","清远市","东莞市","中山市","潮州市","揭阳市","云浮市"};
	/**
	 * 海南省
	 * 海南省共有4个地级市
	 * 4个地级市：分别是海口市、三亚市、三沙市、儋州市
	 */
	final String haiNan[] = {"海口市","三亚市","三沙市","儋州市"};
	/**
	 * 四川省
	 * 四川省共有1个副省级市、17个地级市、3个自治州
	 * 1个副省级市: 成都市
	 * 17个地级市：分别是自贡市、攀枝花市、泸州市、德阳市、绵阳市、广元市、遂宁市、内江市、乐山市、南充市、宜宾市、广安市、达州市、眉山市、雅安市、巴中市、资阳市
	 * 3个自治州：阿坝藏族羌族自治州、甘孜藏族自治州、凉山彝族自治州
	 */
	final String siChuan[] = {"成都市","自贡市","攀枝花市","泸州市","德阳市","绵阳市","广元市","遂宁市","内江市","乐山市","南充市","宜宾市","广安市","达州市","眉山市","雅安市","巴中市","资阳市","阿坝藏族羌族自治州","甘孜藏族自治州","凉山彝族自治州"};
	/**
	 * 贵州省
	 * 贵州省共有6个地级市、3个自治州
	 * 6个地级市：分别是贵阳市、遵义市、六盘水市、安顺市、毕节市、铜仁市
	 * 3个自治州：黔东南苗族侗族自治州、黔南布依族苗族自治州、黔西南布依族苗族自治州
	 */
	final String guiZhou[] = {"贵阳市","遵义市","六盘水市","安顺市","毕节市","铜仁市","黔东南苗族侗族自治州","黔南布依族苗族自治州","黔西南布依族苗族自治州"};
	/**
	 * 云南省
	 * 云南省共有8个地级市、8个少数民族自治州
	 * 8个地级市：分别是昆明市，曲靖市，玉溪市，昭通市，保山市，丽江市，普洱市，临沧市
	 * 8个少数民族自治州：德宏傣族景颇族自治州 ，怒江傈僳族自治州 ，迪庆藏族自治州 ，大理白族自治州 ，楚雄彝族自治州 ，红河哈尼族彝族自治州 ，文山壮族苗族自治州 ，西双版纳傣族自治州
	 */
	final String yunNan[] = {"昆明市","曲靖市","玉溪市","昭通市","保山市","丽江市","普洱市","临沧市","德宏傣族景颇族自治州","怒江傈僳族自治州","迪庆藏族自治州","大理白族自治州","楚雄彝族自治州","红河哈尼族彝族自治州","文山壮族苗族自治州","西双版纳傣族自治州"};
	/**
	 * 陕西省
	 * 陕西省共有10个地级市
	 * 10个地级市：分别是西安市、咸阳市、宝鸡市、渭南市、铜川市、延安市、榆林市、汉中市、安康市、商洛市
	 */
	final String shaanXi[] = {"西安市","咸阳市","宝鸡市","渭南市","铜川市","延安市","榆林市","汉中市","安康市","商洛市"};
	/**
	 * 甘肃省
	 * 甘肃省共有12个地级市、2个自治州
	 * 12个地级市：分别是兰州市、嘉峪关市、金昌市、白银市、天水市、酒泉市、张掖市、武威市、定西市、陇南市、平凉市、庆阳市
	 * 2个自治州：宁夏回族自治州、甘南藏族自治州
	 */
	final String ganSu[] = {"兰州市","嘉峪关市","金昌市","白银市","天水市","酒泉市","张掖市","武威市","定西市","陇南市","平凉市","庆阳市","宁夏回族自治州","甘南藏族自治州"};
	/**
	 * 青海省
	 * 青海省共有2个地级市、6个自治州
	 * 2个地级市：分别是西宁市、海东市
	 * 6个自治州：海北藏族自治州、黄南藏族自治州、海南藏族自治州、果洛藏族自治州、玉树藏族自治州、海西蒙古族藏族自治州
	 */
	final String qingHai[] = {"西宁市","海东市","海北藏族自治州","黄南藏族自治州","海南藏族自治州","果洛藏族自治州","玉树藏族自治州","海西蒙古族藏族自治州"};
	/**
	 * 台湾省
	 * 台湾省共有6个直辖市、3个市
	 * 6个直辖市：分别是台北市、新北市、桃园市、台中市、台南市、高雄市
	 * 3个市：基隆市、新竹市、嘉义市
	 */
	final String taiWan[] = {"台北市","新北市","桃园市","台中市","台南市","高雄市","基隆市","新竹市","嘉义市"};
	/**
	 * 内蒙古自治区
	 * 内蒙古自治区共有9个地级市、3个盟
	 * 9个地级市：分别是呼和浩特市、包头市、乌海市、赤峰市、通辽市、鄂尔多斯市、呼伦贝尔市、巴彦淖尔市、乌兰察布市
	 * 3个盟：兴安盟、锡林郭勒盟、阿拉善盟
	 */
	final String neiMengGu[] = {"呼和浩特市","包头市","乌海市","赤峰市","通辽市","鄂尔多斯市","呼伦贝尔市","巴彦淖尔市","乌兰察布市","兴安盟","锡林郭勒盟","阿拉善盟"};
	/**
	 * 广西壮族自治区
	 * 广西壮族自治区共有14个地级市
	 * 14个地级市：分别是南宁市、柳州市、桂林市、梧州市、北海市、防城港市、钦州市、贵港市、玉林市、百色市、贺州市、河池市、来宾市、崇左市
	 */
	final String guangXi[] = {"南宁市","柳州市","桂林市","梧州市","北海市","防城港市","钦州市","贵港市","玉林市","百色市","贺州市","河池市","来宾市","崇左市"};
	/**
	 * 西藏自治区
	 * 西藏自治区共有6个地级市、1个地区
	 * 6个地级市：分别是拉萨市、日喀则市、昌都市、林芝市、山南市、那曲市
	 * 1个地区：阿里地区
	 */
	final String xiZang[] = {"拉萨市","日喀则市","昌都市","林芝市","山南市","那曲市","阿里地区"};
	/**
	 * 宁夏回族自治区
	 * 宁夏回族自治区共有5个地级市
	 * 5个地级市：分别是银川市、石嘴山市、吴忠市、固原市、中卫市
	 */
	final String ningXia[] = {"银川市","石嘴山市","吴忠市","固原市","中卫市"};
	/**
	 * 新疆维吾尔自治区
	 * 新疆维吾尔自治区共有4地级市、5地区、5自治州
	 * 4地级市：分别是乌鲁木齐市、克拉玛依市、吐鲁番市、哈密市
	 * 5地区：阿克苏地区、喀什地区、和田地区、塔城地区、阿勒泰地区
	 * 5自治州：昌吉回族自治州、博尔塔拉蒙古自治州、巴音郭楞蒙古自治州、克孜勒苏柯尔克孜自治州、伊犁哈萨克自治州
	 */
	final String xinJiang[] = {"乌鲁木齐市","克拉玛依市","吐鲁番市","哈密市","阿克苏地区","喀什地区","和田地区","塔城地区","阿勒泰地区","昌吉回族自治州","博尔塔拉蒙古自治州","巴音郭楞蒙古自治州","克孜勒苏柯尔克孜自治州","伊犁哈萨克自治州"};
	
	
	public ChineseCitySimple() {
		this.setCityLoc();
	}
	
	/**
	 * 转换成城市名全称（城市并不一定正确）
	 * 包含：市、自治州、地区、林区、盟
	 * @param city 城市名
	 * @return
	 */
	private String formatCity(String city) {
		if(city == null) {
			return null;
		}
		if(city == "") {
			return "";
		}
		// 2个特别行政区,"香港特别行政区", "澳门特别行政区"
		if(city.equals("香港") || city.equals("香港特别行政区"))
			city = "香港特别行政区";
		if(city.equals("澳门") || city.equals("澳门特别行政区"))
			city = "澳门特别行政区";
		
		// 处理自治州、地区、林区、盟
		switch (city) {
		// 处理自治州
		//吉林省：延边朝鲜族自治州
		case "延边":
		case "延边州":
		case "延边朝鲜族":
		case "延边朝鲜族自治州":
			city = "延边朝鲜族自治州";
			break;
		//湖北省：恩施土家族苗族自治州
		case "恩施":
		case "恩施州":
		case "恩施土家族":
		case "恩施苗族":
		case "恩施土家族苗族":
		case "恩施苗族土家族":
		case "恩施土家族苗族自治州":
		case "恩施苗族土家族自治州":
			city = "恩施土家族苗族自治州";
			break;
		//湖南省：湘西土家族苗族自治州
		case "湘西":
		case "湘西州":
		case "湘西土家族":
		case "湘西苗族":
		case "湘西土家族苗族":
		case "湘西苗族土家族":
		case "湘西土家族苗族自治州":
		case "湘西苗族土家族自治州":
			city = "湘西土家族苗族自治州";
			break;
		//四川省：阿坝藏族羌族自治州、甘孜藏族自治州、凉山彝族自治州
		case "阿坝":
		case "阿坝州":
		case "阿坝藏族":
		case "阿坝羌族":
		case "阿坝藏族羌族":
		case "阿坝羌族藏族":
		case "阿坝藏族羌族自治州":
		case "阿坝羌族藏族自治州":
			city = "阿坝藏族羌族自治州";
			break;
		case "甘孜":
		case "甘孜州":
		case "甘孜藏族":
		case "甘孜藏族自治州":
			city = "甘孜藏族自治州";
			break;
		case "凉山":
		case "凉山州":
		case "凉山彝族":
		case "凉山彝族自治州":
			city = "凉山彝族自治州";
			break;
		//贵州省：黔东南苗族侗族自治州、黔南布依族苗族自治州、黔西南布依族苗族自治州
		case "黔东南":
		case "黔东南州":
		case "黔东南苗族":
		case "黔东南侗族":
		case "黔东南苗族侗族":
		case "黔东南侗族苗族":
		case "黔东南苗族侗族自治州":
		case "黔东南侗族苗族自治州":
			city = "黔东南苗族侗族自治州";
			break;
		case "黔南":
		case "黔南州":
		case "黔南布依族":
		case "黔南苗族":
		case "黔南布依族苗族":
		case "黔南布苗族依族":
		case "黔南布依族苗族自治州":
		case "黔南布苗族依族自治州":
			city = "黔南布依族苗族自治州";
			break;
		case "黔西南":
		case "黔西南州":
		case "黔西南布依族":
		case "黔西南苗族":
		case "黔西南布依族苗族":
		case "黔西南苗族布依族":
		case "黔西南布依族苗族自治州":
		case "黔西南苗族布依族自治州":
			city = "黔西南布依族苗族自治州";
			break;
		//云南省：德宏傣族景颇族自治州 ，怒江傈僳族自治州 ，迪庆藏族自治州 ，大理白族自治州 ，楚雄彝族自治州 ，红河哈尼族彝族自治州 ，文山壮族苗族自治州 ，西双版纳傣族自治州
		case "德宏":
		case "德宏州":
		case "德宏傣族":
		case "德宏景颇族":
		case "德宏傣族景颇族":
		case "德宏景颇族傣族":
		case "德宏傣族景颇族自治州":
		case "德宏景颇族傣族自治州":
			city = "德宏傣族景颇族自治州";
			break;
		case "怒江":
		case "怒江州":
		case "怒江傈僳族":
		case "怒江傈僳族自治州":
			city = "怒江傈僳族自治州";
			break;
		case "迪庆":
		case "迪庆州":
		case "迪庆藏族":
		case "迪庆藏族自治州":
			city = "迪庆藏族自治州";
			break;
		case "大理":
		case "大理州":
		case "大理白族":
		case "大理白族自治州":
			city = "大理白族自治州";
			break;
		case "楚雄":
		case "楚雄州":
		case "楚雄彝族":
		case "楚雄彝族自治州":
			city = "楚雄彝族自治州";
			break;
		case "红河":
		case "红河州":
		case "红河哈尼族":
		case "红河彝族":
		case "红河哈尼族彝族":
		case "红河彝族哈尼族":
		case "红河哈尼族彝族自治州":
		case "红河彝族哈尼族自治州":
			city = "红河哈尼族彝族自治州";
			break;
		case "文山":
		case "文山州":
		case "文山壮族":
		case "文山苗族":
		case "文山壮族苗族":
		case "文山苗族壮族":
		case "文山壮族苗族自治州":
		case "文山苗族壮族自治州":
			city = "文山壮族苗族自治州";
			break;
		case "西双版纳":
		case "勐巴拉娜西":
		case "西双版纳傣族":
		case "西双版纳傣族自治州":
			city = "西双版纳傣族自治州";
			break;
		// 甘肃省：宁夏回族自治州、甘南藏族自治州
		case "宁夏":
		case "宁夏州":
		case "宁夏回族":
		case "宁夏回族自治州": //有个宁夏回族自治区
			city = "宁夏回族自治州";
			break;
		case "甘南":
		case "甘南州":
		case "甘南藏族":
		case "甘南藏族自治州":
			city = "甘南藏族自治州";
			break;
		// 青海省：海北藏族自治州、黄南藏族自治州、海南藏族自治州、果洛藏族自治州、玉树藏族自治州、海西蒙古族藏族自治州
		case "海北"://
		case "海北州":
		case "海北藏族":
		case "海北藏族自治州":
			city = "海北藏族自治州";
			break;
		case "黄南"://
		case "黄南州":	
		case "黄南藏族":
		case "黄南藏族自治州":
			city = "黄南藏族自治州";
			break;
		case "海南": //有个海南省
		case "海南州"://
		case "海南藏族":
		case "海南藏族自治州":
			city = "海南藏族自治州";
			break;
		case "果洛"://
		case "果洛州":
		case "果洛藏族":
		case "果洛藏族自治州":
			city = "果洛藏族自治州";
			break;
		case "玉树"://
		case "玉树州":
		case "玉树藏族":
		case "玉树藏族自治州":
			city = "玉树藏族自治州";
			break;
		case "海西":
		case "海西州":
		case "海西蒙古族":
		case "海西藏族":
		case "海西蒙古族藏族":
		case "海西藏族蒙古族":
		case "海西蒙古族藏族自治州":
		case "海西藏族蒙古族自治州":
			city = "海西蒙古族藏族自治州";
			break;
		// 新疆维吾尔自治区：昌吉回族自治州、博尔塔拉蒙古自治州、巴音郭楞蒙古自治州、克孜勒苏柯尔克孜自治州、伊犁哈萨克自治州
		case "昌吉":
		case "昌吉州":
		case "昌吉回族":
		case "昌吉回族自治州":
			city = "昌吉回族自治州";
			break;
		case "博州":
		case "博尔塔拉":
		case "博尔塔拉蒙古":
		case "博尔塔拉蒙古自治州":
			city = "博尔塔拉蒙古自治州";
			break;
		case "巴州":
		case "巴音郭楞":
		case "巴音郭楞蒙古":
		case "巴音郭楞蒙古自治州":
			city = "巴音郭楞蒙古自治州";
			break;
		
		// 地区
		// 黑龙江省：大兴安岭地区
		case "大兴安岭":
		case "大兴安岭地区":
			city = "大兴安岭地区";
			break;
		//西藏自治区：阿里地区
		case "阿里":
		case "阿里地区":
			city = "阿里地区";
			break;
		// 新疆维吾尔自治区：阿克苏地区、喀什地区、和田地区、塔城地区、阿勒泰地区
		case "阿克苏":
		case "阿克苏地区":
			city = "阿克苏地区";
			break;
		case "喀什":
		case "喀什地区":
			city = "喀什地区";
			break;
		case "和田":
		case "和田地区":
			city = "和田地区";
			break;
		case "塔城":
		case "塔城地区":
			city = "塔城地区";
			break;
		case "阿勒泰":
		case "阿勒泰地区":
			city = "阿勒泰地区";
			break;
		
		//林区
		// 湖北省：神农架林区
		case "神农架":
		case "神农架林区":
			city = "神农架林区";
			break;
		
		// 盟
		// 内蒙古自治区：兴安盟、锡林郭勒盟、阿拉善盟
		case "兴安":
		case "兴安盟":
			city = "兴安盟";
			break;
		case "锡林郭勒":
		case "锡林郭勒盟":
			city = "锡林郭勒盟";
			break;
		case "阿拉善":
		case "阿拉善盟":
			city = "阿拉善盟";
			break;
		}
		
		// 处理市
		if((!city.equals("香港特别行政区")) && (!city.equals("澳门特别行政区")) && (!isContainsField(city))) {
			if(!city.substring(city.length()-1).equals("市"))	{
				city = city.concat("市");
			}
		}
		return city;
	}
	
	// 判断该城市字段是否包含（自治州、地区、林区、盟）这些字段
	public boolean isContainsField(String city) {
		if(city.length()>=3 && city.substring(city.length()-3).equals("自治州"))
			return true;
		if(city.length()>=2 && city.substring(city.length()-2).equals("地区"))
			return true;
		if(city.length()>=2 && city.substring(city.length()-2).equals("林区"))
			return true;
		if(city.length()>=1 && city.substring(city.length()-1).equals("盟"))
			return true;
		return false;
	}
	
	/**
	 * 转换成城市名简称（城市并不一定正确）
	 * 包含：市、自治州、地区、林区、盟
	 * @param city 城市名
	 * @return
	 */
	private String formatCitySimple(String city) {
		if(city == null) {
			return null;
		}
		if(city == "") {
			return "";
		}
		// 2个特别行政区,"香港特别行政区", "澳门特别行政区"
		if(city.equals("香港") || city.equals("香港特别行政区"))
			city = "香港";
		if(city.equals("澳门") || city.equals("澳门特别行政区"))
			city = "澳门";
		
		// 处理自治州、地区、林区、盟
		switch (city) {
		// 处理自治州
		//吉林省：延边朝鲜族自治州
		case "延边":
		case "延边州":
		case "延边朝鲜族":
		case "延边朝鲜族自治州":
			city = "延边";
			break;
		//湖北省：恩施土家族苗族自治州
		case "恩施":
		case "恩施州":
		case "恩施土家族":
		case "恩施苗族":
		case "恩施土家族苗族":
		case "恩施苗族土家族":
		case "恩施土家族苗族自治州":
		case "恩施苗族土家族自治州":
			city = "恩施";
			break;
		//湖南省：湘西土家族苗族自治州
		case "湘西":
		case "湘西州":
		case "湘西土家族":
		case "湘西苗族":
		case "湘西土家族苗族":
		case "湘西苗族土家族":
		case "湘西土家族苗族自治州":
		case "湘西苗族土家族自治州":
			city = "湘西";
			break;
		//四川省：阿坝藏族羌族自治州、甘孜藏族自治州、凉山彝族自治州
		case "阿坝":
		case "阿坝州":
		case "阿坝藏族":
		case "阿坝羌族":
		case "阿坝藏族羌族":
		case "阿坝羌族藏族":
		case "阿坝藏族羌族自治州":
		case "阿坝羌族藏族自治州":
			city = "阿坝";
			break;
		case "甘孜":
		case "甘孜州":
		case "甘孜藏族":
		case "甘孜藏族自治州":
			city = "甘孜";
			break;
		case "凉山":
		case "凉山州":
		case "凉山彝族":
		case "凉山彝族自治州":
			city = "凉山";
			break;
		//贵州省：黔东南苗族侗族自治州、黔南布依族苗族自治州、黔西南布依族苗族自治州
		case "黔东南":
		case "黔东南州":
		case "黔东南苗族":
		case "黔东南侗族":
		case "黔东南苗族侗族":
		case "黔东南侗族苗族":
		case "黔东南苗族侗族自治州":
		case "黔东南侗族苗族自治州":
			city = "黔东南";
			break;
		case "黔南":
		case "黔南州":
		case "黔南布依族":
		case "黔南苗族":
		case "黔南布依族苗族":
		case "黔南布苗族依族":
		case "黔南布依族苗族自治州":
		case "黔南布苗族依族自治州":
			city = "黔南";
			break;
		case "黔西南":
		case "黔西南州":
		case "黔西南布依族":
		case "黔西南苗族":
		case "黔西南布依族苗族":
		case "黔西南苗族布依族":
		case "黔西南布依族苗族自治州":
		case "黔西南苗族布依族自治州":
			city = "黔西南";
			break;
		//云南省：德宏傣族景颇族自治州 ，怒江傈僳族自治州 ，迪庆藏族自治州 ，大理白族自治州 ，楚雄彝族自治州 ，红河哈尼族彝族自治州 ，文山壮族苗族自治州 ，西双版纳傣族自治州
		case "德宏":
		case "德宏州":
		case "德宏傣族":
		case "德宏景颇族":
		case "德宏傣族景颇族":
		case "德宏景颇族傣族":
		case "德宏傣族景颇族自治州":
		case "德宏景颇族傣族自治州":
			city = "德宏";
			break;
		case "怒江":
		case "怒江州":
		case "怒江傈僳族":
		case "怒江傈僳族自治州":
			city = "怒江";
			break;
		case "迪庆":
		case "迪庆州":
		case "迪庆藏族":
		case "迪庆藏族自治州":
			city = "迪庆";
			break;
		case "大理":
		case "大理州":
		case "大理白族":
		case "大理白族自治州":
			city = "大理";
			break;
		case "楚雄":
		case "楚雄州":
		case "楚雄彝族":
		case "楚雄彝族自治州":
			city = "楚雄";
			break;
		case "红河":
		case "红河州":
		case "红河哈尼族":
		case "红河彝族":
		case "红河哈尼族彝族":
		case "红河彝族哈尼族":
		case "红河哈尼族彝族自治州":
		case "红河彝族哈尼族自治州":
			city = "红河";
			break;
		case "文山":
		case "文山州":
		case "文山壮族":
		case "文山苗族":
		case "文山壮族苗族":
		case "文山苗族壮族":
		case "文山壮族苗族自治州":
		case "文山苗族壮族自治州":
			city = "文山";
			break;
		case "西双版纳":
		case "勐巴拉娜西":
		case "西双版纳傣族":
		case "西双版纳傣族自治州":
			city = "西双版纳";
			break;
		// 甘肃省：宁夏回族自治州、甘南藏族自治州
		case "宁夏":
		case "宁夏州":
		case "宁夏回族":
		case "宁夏回族自治州": //有个宁夏回族自治区
			city = "宁夏州";
			break;
		case "甘南":
		case "甘南州":
		case "甘南藏族":
		case "甘南藏族自治州":
			city = "甘南";
			break;
		// 青海省：海北藏族自治州、黄南藏族自治州、海南藏族自治州、果洛藏族自治州、玉树藏族自治州、海西蒙古族藏族自治州
		case "海北"://
		case "海北州":
		case "海北藏族":
		case "海北藏族自治州":
			city = "海北";
			break;
		case "黄南"://
		case "黄南州":	
		case "黄南藏族":
		case "黄南藏族自治州":
			city = "黄南";
			break;
		case "海南": //有个海南省
		case "海南州"://
		case "海南藏族":
		case "海南藏族自治州":
			city = "海南州";
			break;
		case "果洛"://
		case "果洛州":
		case "果洛藏族":
		case "果洛藏族自治州":
			city = "果洛";
			break;
		case "玉树"://
		case "玉树州":
		case "玉树藏族":
		case "玉树藏族自治州":
			city = "玉树";
			break;
		case "海西":
		case "海西州":
		case "海西蒙古族":
		case "海西藏族":
		case "海西蒙古族藏族":
		case "海西藏族蒙古族":
		case "海西蒙古族藏族自治州":
		case "海西藏族蒙古族自治州":
			city = "海西";
			break;
		// 新疆维吾尔自治区：昌吉回族自治州、博尔塔拉蒙古自治州、巴音郭楞蒙古自治州、克孜勒苏柯尔克孜自治州、伊犁哈萨克自治州
		case "昌吉":
		case "昌吉州":
		case "昌吉回族":
		case "昌吉回族自治州":
			city = "昌吉";
			break;
		case "博州":
		case "博尔塔拉":
		case "博尔塔拉蒙古":
		case "博尔塔拉蒙古自治州":
			city = "博州";
			break;
		case "巴州":
		case "巴音郭楞":
		case "巴音郭楞蒙古":
		case "巴音郭楞蒙古自治州":
			city = "巴州";
			break;
		
		// 地区
		// 黑龙江省：大兴安岭地区
		case "大兴安岭":
		case "大兴安岭地区":
			city = "大兴安岭";
			break;
		//西藏自治区：阿里地区
		case "阿里":
		case "阿里地区":
			city = "阿里";
			break;
		// 新疆维吾尔自治区：阿克苏地区、喀什地区、和田地区、塔城地区、阿勒泰地区
		case "阿克苏":
		case "阿克苏地区":
			city = "阿克苏";
			break;
		case "喀什":
		case "喀什地区":
			city = "喀什";
			break;
		case "和田":
		case "和田地区":
			city = "和田";
			break;
		case "塔城":
		case "塔城地区":
			city = "塔城";
			break;
		case "阿勒泰":
		case "阿勒泰地区":
			city = "阿勒泰";
			break;
		
		//林区
		// 湖北省：神农架林区
		case "神农架":
		case "神农架林区":
			city = "神农架";
			break;
		
		// 盟
		// 内蒙古自治区：兴安盟、锡林郭勒盟、阿拉善盟
		case "兴安":
		case "兴安盟":
			city = "兴安";
			break;
		case "锡林郭勒":
		case "锡林郭勒盟":
			city = "锡林郭勒";
			break;
		case "阿拉善":
		case "阿拉善盟":
			city = "阿拉善";
			break;
		}
		
		// 处理市
		if (city.substring(city.length() - 1).equals("市")) {
			city = city.substring(0, city.length()-1);
		}
		return city;
	}
	
	/**
	 * 判断是否是中国城市
	 * @param city 城市
	 * @return -1表示为空，0表示城市错误或者不是中国城市，1表示是中国城市
	 */
	private int isChineseCity2(String city) {
		// 先格式化市
		city = formatCity(city);
		if(city == null || city == "") {
			return -1;
		}
		for(String str : municipality) {
			if(city.equals(str))
				return 1;
		}
		for(String str : specialAdminRegion) {
			if(city.equals(str))
				return 1;
		}
		for(String str : heBei) {
			if(city.equals(str))
				return 1;
		}
		for(String str : shanXi) {
			if(city.equals(str))
				return 1;
		}
		for(String str : liaoNing) {
			if(city.equals(str))
				return 1;
		}
		for(String str : jiLin) {
			if(city.equals(str))
				return 1;
		}
		for(String str : heiLongJiang) {
			if(city.equals(str))
				return 1;
		}
		for(String str : jiangSu) {
			if(city.equals(str))
				return 1;
		}
		for(String str : zheJiang) {
			if(city.equals(str))
				return 1;
		}
		for(String str : anHui) {
			if(city.equals(str))
				return 1;
		}
		for(String str : fuJian) {
			if(city.equals(str))
				return 1;
		}
		for(String str : jiangXi) {
			if(city.equals(str))
				return 1;
		}
		for(String str : shanDong) {
			if(city.equals(str))
				return 1;
		}
		for(String str : heNan) {
			if(city.equals(str))
				return 1;
		}
		for(String str : huBei) {
			if(city.equals(str))
				return 1;
		}
		for(String str : huNan) {
			if(city.equals(str))
				return 1;
		}
		for(String str : guangDong) {
			if(city.equals(str))
				return 1;
		}
		for(String str : haiNan) {
			if(city.equals(str))
				return 1;
		}
		for(String str : siChuan) {
			if(city.equals(str))
				return 1;
		}
		for(String str : guiZhou) {
			if(city.equals(str))
				return 1;
		}
		for(String str : yunNan) {
			if(city.equals(str))
				return 1;
		}
		for(String str : shaanXi) {
			if(city.equals(str))
				return 1;
		}
		for(String str : ganSu) {
			if(city.equals(str))
				return 1;
		}
		for(String str : qingHai) {
			if(city.equals(str))
				return 1;
		}
		for(String str : taiWan) {
			if(city.equals(str))
				return 1;
		}
		for(String str : neiMengGu) {
			if(city.equals(str))
				return 1;
		}
		for(String str : guangXi) {
			if(city.equals(str))
				return 1;
		}
		for(String str : xiZang) {
			if(city.equals(str))
				return 1;
		}
		for(String str : ningXia) {
			if(city.equals(str))
				return 1;
		}
		for(String str : xinJiang) {
			if(city.equals(str))
				return 1;
		}
		return 0;
	}
	
	/**
	 * 判断是否是中国城市
	 * @param city 城市
	 * @return 是返回true，不是抛出异常
	 */
	public boolean isChineseCity(String city) {
		if(isChineseCity2(city)==-1)
			throw new IllegalArgumentException("城市不应为空");
		else if(isChineseCity2(city)==0)
			throw new IllegalArgumentException("城市错误或者不是中国城市");
		else
			return true;
	}
	
	/**
	 * 该城市属于哪个省
	 * @param city 城市名称
	 * @return 省
	 */
	public String whichProvince(String city) {
		// 先格式化市
		city = formatCity(city);
		
		if(isChineseCity(city)) {
			for(String str : municipality) {
				if(city.equals(str))
					return city;
			}
			for(String str : specialAdminRegion) {
				if(city.equals(str))
					return city;
			}
			for(String str : heBei) {
				if(city.equals(str))
					return "河北省";
			}
			for(String str : shanXi) {
				if(city.equals(str))
					return "山西省";
			}
			for(String str : liaoNing) {
				if(city.equals(str))
					return "辽宁省";
			}
			for(String str : jiLin) {
				if(city.equals(str))
					return "吉林省";
			}
			for(String str : heiLongJiang) {
				if(city.equals(str))
					return "黑龙江省";
			}
			for(String str : jiangSu) {
				if(city.equals(str))
					return "江苏省";
			}
			for(String str : zheJiang) {
				if(city.equals(str))
					return "浙江省";
			}
			for(String str : anHui) {
				if(city.equals(str))
					return "安徽省";
			}
			for(String str : fuJian) {
				if(city.equals(str))
					return "福建省";
			}
			for(String str : jiangXi) {
				if(city.equals(str))
					return "江西省";
			}
			for(String str : shanDong) {
				if(city.equals(str))
					return "山东省";
			}
			for(String str : heNan) {
				if(city.equals(str))
					return "河南省";
			}
			for(String str : huBei) {
				if(city.equals(str))
					return "湖北省";
			}
			for(String str : huNan) {
				if(city.equals(str))
					return "湖南省";
			}
			for(String str : guangDong) {
				if(city.equals(str))
					return "广东省";
			}
			for(String str : haiNan) {
				if(city.equals(str))
					return "海南省";
			}
			for(String str : siChuan) {
				if(city.equals(str))
					return "四川省";
			}
			for(String str : guiZhou) {
				if(city.equals(str))
					return "贵州省";
			}
			for(String str : yunNan) {
				if(city.equals(str))
					return "云南省";
			}
			for(String str : shaanXi) {
				if(city.equals(str))
					return "陕西省";
			}
			for(String str : ganSu) {
				if(city.equals(str))
					return "甘肃省";
			}
			for(String str : qingHai) {
				if(city.equals(str))
					return "青海省";
			}
			for(String str : taiWan) {
				if(city.equals(str))
					return "台湾省";
			}
			for(String str : neiMengGu) {
				if(city.equals(str))
					return "内蒙古自治区";
			}
			for(String str : guangXi) {
				if(city.equals(str))
					return "广西壮族自治区";
			}
			for(String str : xiZang) {
				if(city.equals(str))
					return "西藏自治区";
			}
			for(String str : ningXia) {
				if(city.equals(str))
					return "宁夏回族自治区";
			}
			for(String str : xinJiang) {
				if(city.equals(str))
					return "新疆维吾尔自治区";
			}
		}	
		return "不是中国城市";
	}
	
	// 设置中国所有城市的经纬度
	// http://api.map.baidu.com/geocoder?address=城市名称&output=json&key=37492c0ee6f924cb5e934fa08c6b1676&city=北京市
	private void setCityLoc() {
		// 4个直辖市："北京市", "天津市", "上海市", "重庆市"
		cityLoc.put("北京市", new double[] {116.395645,39.929986});
		cityLoc.put("天津市", new double[] {117.210813,39.14393});
		cityLoc.put("上海市", new double[] {117.210813,39.14393});
		cityLoc.put("重庆市", new double[] {106.530635,29.544606});
		// 2个特别行政区："香港特别行政区", "澳门特别行政区"
		cityLoc.put("香港特别行政区", new double[] {114.186124,22.293586});
		cityLoc.put("澳门特别行政区", new double[] {113.557519,22.204118});
		
		//包括23个省，5个自治区 又包含多个市
		//河北省11个地级市：石家庄市，唐山市，秦皇岛市，邯郸市，邢台市，保定市，张家口市，承德市，沧州市，廊坊市，衡水市
		cityLoc.put("石家庄市", new double[] {114.522082,38.048958});
		cityLoc.put("唐山市", new double[] {118.183451,39.650531});
		cityLoc.put("秦皇岛市", new double[] {119.604368,39.945462});
		cityLoc.put("邯郸市", new double[] {114.482694,36.609308});
		cityLoc.put("邢台市", new double[] {114.482694,37.069531});
		cityLoc.put("保定市", new double[] {115.49481,38.886565});
		cityLoc.put("张家口市", new double[] {114.893782,40.811188});
		cityLoc.put("承德市", new double[] {117.933822,40.992521});
		cityLoc.put("沧州市", new double[] {116.863806,38.297615});
		cityLoc.put("廊坊市", new double[] {116.703602,39.518611});
		cityLoc.put("衡水市", new double[] {115.686229,37.746929});
		//山西省11个地级市：太原市、大同市、朔州市、阳泉市、长治市、忻州市、吕梁市、晋中市、临汾市、运城市、晋城市
		cityLoc.put("太原市", new double[] {112.550864,37.890277});
		cityLoc.put("大同市", new double[] {113.290509,40.113744});
		cityLoc.put("朔州市", new double[] {112.479928,39.337672});
		cityLoc.put("阳泉市", new double[] {113.569238,37.869529});
		cityLoc.put("长治市", new double[] {113.120292,36.201664});
		cityLoc.put("忻州市", new double[] {112.727939,38.461031});
		cityLoc.put("吕梁市", new double[] {111.143157,37.527316});
		cityLoc.put("晋中市", new double[] {112.738514,37.693362});
		cityLoc.put("临汾市", new double[] {111.538788,36.099745});
		cityLoc.put("运城市", new double[] {111.006854,35.038859});
		cityLoc.put("晋城市", new double[] {112.867333,35.499834});
		//辽宁省14个地级市：沈阳市、大连市、鞍山市、抚顺市、本溪市、丹东市、锦州市、
		// 营口市、阜新市、辽阳市、盘锦市、铁岭市、朝阳市、葫芦岛市
		cityLoc.put("沈阳市", new double[] {112.867333,41.808645});
		cityLoc.put("大连市", new double[] {121.593478,38.94871});
		cityLoc.put("鞍山市", new double[] {123.007763,41.118744});
		cityLoc.put("抚顺市", new double[] {123.92982,41.877304});
		cityLoc.put("本溪市", new double[] {123.778062,41.325838});
		cityLoc.put("丹东市", new double[] {124.338543,40.129023});
		cityLoc.put("锦州市", new double[] {121.147749,41.130879});
		cityLoc.put("营口市", new double[] {122.233391,40.668651});
		cityLoc.put("阜新市", new double[] {121.660822,42.01925});
		cityLoc.put("辽阳市", new double[] {123.172451,41.273339});
		cityLoc.put("盘锦市", new double[] {122.073228,41.141248});
		cityLoc.put("铁岭市", new double[] {123.85485,42.299757});
		cityLoc.put("朝阳市", new double[] {120.446163,41.571828});
		cityLoc.put("葫芦岛市", new double[] {120.860758,40.74303});
		// 吉林省一共有8个地级市：长春市、吉林市、四平市、辽源市、通化市、白山市、松原市、白城市
		// 1个自治州：延边朝鲜族自治州
		cityLoc.put("长春市", new double[] {125.313642,43.898338});
		cityLoc.put("吉林市", new double[] {126.564544,43.871988});
		cityLoc.put("四平市", new double[] {124.391382,43.175525});
		cityLoc.put("辽源市", new double[] {125.133686,42.923303});
		cityLoc.put("通化市", new double[] {125.94265,41.736397});
		cityLoc.put("白山市", new double[] {126.435798,41.945859});
		cityLoc.put("松原市", new double[] {124.832995,45.136049});
		cityLoc.put("白城市", new double[] {122.840777,45.621086});
		cityLoc.put("延边朝鲜族自治州", new double[] {129.485902,42.896414});
		//黑龙江共有12个地级市1个地区。
		//12个地级市：分别是哈尔滨市，齐齐哈尔市，牡丹江市，佳木斯市，大庆市，鸡西市，双鸭山市，鹤岗市，伊春市，七台河市，黑河市，绥化市
		// 1个地区：大兴安岭地区
		cityLoc.put("哈尔滨市", new double[] {126.657717,45.773225});
		cityLoc.put("齐齐哈尔市", new double[] {123.987289,47.3477});
		cityLoc.put("牡丹江市", new double[] {129.608035,44.588521});
		cityLoc.put("佳木斯市", new double[] {130.284735,46.81378});
		cityLoc.put("大庆市", new double[] {125.02184,46.596709});
		cityLoc.put("鸡西市", new double[] {130.941767,45.32154});
		cityLoc.put("双鸭山市", new double[] {131.171402,46.655102});
		cityLoc.put("鹤岗市", new double[] {130.292472,47.338666});
		cityLoc.put("伊春市", new double[] {128.910766,47.734685});
		cityLoc.put("七台河市", new double[] {131.019048,45.775005});
		cityLoc.put("黑河市", new double[] {127.50083,50.25069});
		cityLoc.put("绥化市", new double[] {126.989095,46.646064});
		cityLoc.put("大兴安岭地区", new double[] {124.196104,51.991789});
		//江苏省共有13个地级市。
		//13个地级市：分别是南京市、徐州市、常州市、淮安市、南通市、宿迁市、无锡市、扬州市、盐城市、镇江市、泰州市、苏州市、连云港市
		cityLoc.put("南京市", new double[] {118.778074,32.057236});
		cityLoc.put("徐州市", new double[] {117.188107,34.271553});
		cityLoc.put("常州市", new double[] {119.981861,31.771397});
		cityLoc.put("淮安市", new double[] {119.030186,33.606513});
		cityLoc.put("南通市", new double[] {120.873801,32.014665});
		cityLoc.put("宿迁市", new double[] {118.296893,33.95205});
		cityLoc.put("无锡市", new double[] {120.305456,31.570037});
		cityLoc.put("扬州市", new double[] {119.427778,32.408505});
		cityLoc.put("盐城市", new double[] {120.148872,33.379862});
		cityLoc.put("镇江市", new double[] {119.455835,32.204409});
		cityLoc.put("泰州市", new double[] {119.919606,32.476053});
		cityLoc.put("苏州市", new double[] {120.619907,31.317987});
		cityLoc.put("连云港市", new double[] {119.173872,34.601549});
		//浙江省共有11个地级市。
		// 11个地级市：分别是杭州市、宁波市、温州市、绍兴市、湖州市、嘉兴市、金华市、衢州市、台州市、丽水市、舟山市
		cityLoc.put("杭州市", new double[] {120.219375,30.259244});
		cityLoc.put("杭州市", new double[] {121.579006,29.885259});
		cityLoc.put("温州市", new double[] {120.690635,28.002838});
		cityLoc.put("绍兴市", new double[] {120.592467,30.002365});
		cityLoc.put("湖州市", new double[] {120.137243,30.877925});
		cityLoc.put("嘉兴市", new double[] {120.760428,30.773992});
		cityLoc.put("金华市", new double[] {119.652576,29.102899});
		cityLoc.put("衢州市", new double[] {118.875842,28.95691});
		cityLoc.put("台州市", new double[] {121.440613,28.668283});
		cityLoc.put("丽水市", new double[] {119.929576,28.4563});
		cityLoc.put("舟山市", new double[] {122.169872,30.03601});
		//安徽省共有17个地级市。
		// 17个地级市：分别是合肥市、蚌埠市、芜湖市、淮南市、马鞍山市、淮北市、铜陵市、安庆市、黄山市、阜阳市、宿州市、滁州市、六安市、宣城市、巢湖市、池州市、亳州市
		cityLoc.put("合肥市", new double[] {117.282699,31.866942});
		cityLoc.put("蚌埠市", new double[] {117.35708,32.929499});
		cityLoc.put("芜湖市", new double[] {118.384108,31.36602});
		cityLoc.put("淮南市", new double[] {117.018639,32.642812});
		cityLoc.put("马鞍山市", new double[] {118.515882,31.688528});
		cityLoc.put("淮北市", new double[] {116.791447,33.960023});
		cityLoc.put("铜陵市", new double[] {117.819429,30.94093});
		cityLoc.put("安庆市", new double[] {117.058739,30.537898});
		cityLoc.put("黄山市", new double[] {118.29357,29.734435});
		cityLoc.put("阜阳市", new double[] {115.820932,32.901211});
		cityLoc.put("宿州市", new double[] {116.988692,33.636772});
		cityLoc.put("滁州市", new double[] {118.32457,32.317351});
		cityLoc.put("六安市", new double[] {116.505253,31.755558});
		cityLoc.put("宣城市", new double[] {118.752096,30.951642});
		cityLoc.put("巢湖市", new double[] {117.88049,31.608733});
		cityLoc.put("池州市", new double[] {117.494477,30.660019});
		cityLoc.put("亳州市", new double[] {115.787928,33.871211});
		//福建省共有9地级市。
		// 9地级市：分别是福州市、龙岩市、南平市、宁德市、莆田市、泉州市、三明市、厦门市、漳州市。
		cityLoc.put("福州市", new double[] {119.330221,26.047125});
		cityLoc.put("龙岩市", new double[] {117.017997,25.078685});
		cityLoc.put("南平市", new double[] {118.181883,26.643626});
		cityLoc.put("宁德市", new double[] {119.542082,26.656527});
		cityLoc.put("莆田市", new double[] {119.077731,25.44845});
		cityLoc.put("泉州市", new double[] {118.600362,24.901652});
		cityLoc.put("三明市", new double[] {117.642194,26.270835});
		cityLoc.put("厦门市", new double[] {118.103886,24.489231});
		cityLoc.put("漳州市", new double[] {117.676205,24.517065});
		//江西省共有11地级市。
		//11地级市：分别是南昌市、赣州市、吉安市、萍乡市、宜春市、新余市、景德镇市、抚州市、九江市、上饶市、鹰潭市。
		cityLoc.put("南昌市", new double[] {115.893528,28.689578});
		cityLoc.put("赣州市", new double[] {114.935909,25.845296});
		cityLoc.put("吉安市", new double[] {114.992039,27.113848});
		cityLoc.put("萍乡市", new double[] {113.859917,27.639544});
		cityLoc.put("宜春市", new double[] {114.400039,27.81113});
		cityLoc.put("新余市", new double[] {114.947117,27.822322});
		cityLoc.put("景德镇市", new double[] {117.186523,29.303563});
		cityLoc.put("抚州市", new double[] {116.360919,27.954545});
		cityLoc.put("九江市", new double[] {115.999848,29.71964});
		cityLoc.put("上饶市", new double[] {117.955464,28.457623});
		cityLoc.put("鹰潭市", new double[] {117.03545,28.24131});
		//山东省共有17地级市。
		//17地级市：分别是济南市、青岛市、烟台市、威海市、东营市、淄博市、潍坊市、日照市、莱芜市、菏泽市、枣庄市、德州市、滨州市、临沂市、济宁市、聊城市、泰安市
		cityLoc.put("济南市", new double[] {117.024967,36.682785});
		cityLoc.put("青岛市", new double[] {120.384428,36.105215});
		cityLoc.put("烟台市", new double[] {121.309555,37.536562});
		cityLoc.put("威海市", new double[] {122.093958,37.528787});
		cityLoc.put("东营市", new double[] {118.583926,37.487121});
		cityLoc.put("淄博市", new double[] {118.059134,36.804685});
		cityLoc.put("潍坊市", new double[] {119.142634,36.716115});
		cityLoc.put("日照市", new double[] {119.50718,35.420225});
		cityLoc.put("莱芜市", new double[] {117.684667,36.233654});
		cityLoc.put("菏泽市", new double[] {115.46336,35.26244});
		cityLoc.put("枣庄市", new double[] {117.279305,34.807883});
		cityLoc.put("德州市", new double[] {116.328161,37.460826});
		cityLoc.put("滨州市", new double[] {117.968292,37.405314});
		cityLoc.put("临沂市", new double[] {118.340768,35.072409});
		cityLoc.put("济宁市", new double[] {116.600798,35.402122});
		cityLoc.put("聊城市", new double[] {115.986869,36.455829});
		cityLoc.put("泰安市", new double[] {117.089415,36.188078});
		//河南省共有17个地级市、1个省直辖县级行政单位
		// 17地级市：分别是郑州市、开封市、洛阳市、平顶山市、安阳市、鹤壁市、新乡市、焦作市、濮阳市、许昌市、漯河市、三门峡市、南阳市、商丘市、信阳市、周口市、驻马店市
		//1个省直辖县级行政单位：济源市
		cityLoc.put("郑州市", new double[] {113.649644,34.75661});
		cityLoc.put("开封市", new double[] {114.351642,34.801854});
		cityLoc.put("洛阳市", new double[] {112.447525,34.657368});
		cityLoc.put("平顶山市", new double[] {113.300849,33.745301});
		cityLoc.put("安阳市", new double[] {114.351807,36.110267});
		cityLoc.put("鹤壁市", new double[] {114.29777,35.755426});
		cityLoc.put("新乡市", new double[] {113.91269,35.307258});
		cityLoc.put("焦作市", new double[] {113.211836,35.234608});
		cityLoc.put("濮阳市", new double[] {115.026627,35.753298});
		cityLoc.put("许昌市", new double[] {113.835312,34.02674});
		cityLoc.put("漯河市", new double[] {114.046061,33.576279});
		cityLoc.put("三门峡市", new double[] {111.181262,34.78332});
		cityLoc.put("南阳市", new double[] {112.542842,33.01142});
		cityLoc.put("商丘市", new double[] {115.641886,34.438589});
		cityLoc.put("信阳市", new double[] {114.085491,32.128582});
		cityLoc.put("周口市", new double[] {114.654102,33.623741});
		cityLoc.put("驻马店市", new double[] {114.049154,32.983158});
		cityLoc.put("济源市", new double[] {112.40383,35.093893});
		// * 湖北省共有17个地级市、1个省直辖县级行政单位
		// 湖北省现下辖：1个副省级市（武汉市）；
		// 11个地级市（黄石市、十堰市、荆州市、宜昌市、襄阳市、鄂州市、荆门市、黄冈市、孝感市、咸宁市、随州市）；
		// 1个自治州（恩施土家族苗族自治州）；
		// 3个直管市（仙桃市、天门市、潜江市）；
		// 1个林区（神农架林区）。
		cityLoc.put("武汉市", new double[] {114.3162,30.581084});
		cityLoc.put("黄石市", new double[] {115.050683,30.216127});
		cityLoc.put("十堰市", new double[] {110.801229,32.636994});
		cityLoc.put("荆州市", new double[] {112.241866,30.332591});
		cityLoc.put("宜昌市", new double[] {111.310981,30.732758});
		cityLoc.put("襄阳市", new double[] {111.949549,31.939713});
		cityLoc.put("鄂州市", new double[] {114.895594,30.384439});
		cityLoc.put("荆门市", new double[] {112.21733,31.042611});
		cityLoc.put("黄冈市", new double[] {114.906618,30.446109});
		cityLoc.put("孝感市", new double[] {113.935734,30.927955});
		cityLoc.put("咸宁市", new double[] {114.300061,29.880657});
		cityLoc.put("随州市", new double[] {113.379358,31.717858});
		cityLoc.put("恩施土家族苗族自治州", new double[] {109.491923,30.285888});
		cityLoc.put("仙桃市", new double[] {113.387448,30.293966});
		cityLoc.put("天门市", new double[] {113.12623,30.649047});
		cityLoc.put("潜江市", new double[] {112.768768,30.343116});
		cityLoc.put("神农架林区", new double[] {110.487231,31.595768});
		//湖南省共有13个地级市、1个自治州
		//13个地级市：分别是长沙市, 株洲市, 湘潭市, 衡阳市, 永州市, 邵阳市, 岳阳市, 张家界市, 常德市, 益阳市, 娄底市, 郴州市, 怀化市
		// 1个自治州： 湘西土家族苗族自治州
		cityLoc.put("长沙市", new double[] {112.979353,28.213478});
		cityLoc.put("株洲市", new double[] {113.131695,27.827433});
		cityLoc.put("湘潭市", new double[] {112.935556,27.835095});
		cityLoc.put("衡阳市", new double[] {112.583819,26.898164});
		cityLoc.put("永州市", new double[] {111.614648,26.435972});
		cityLoc.put("邵阳市", new double[] {111.461525,27.236811});
		cityLoc.put("岳阳市", new double[] {113.146196,29.378007});
		cityLoc.put("张家界市", new double[] {110.48162,29.124889});
		cityLoc.put("常德市", new double[] {111.653718,29.012149});
		cityLoc.put("益阳市", new double[] {112.366547,28.588088});
		cityLoc.put("娄底市", new double[] {111.996396,27.741073});
		cityLoc.put("郴州市", new double[] {113.037704,25.782264});
		cityLoc.put("怀化市", new double[] {109.986959,27.557483});
		cityLoc.put("湘西土家族苗族自治州", new double[] {109.745746,28.317951});
		//广东省共有21个地级市
		// 21个地级市：分别是广州市、深圳市、珠海市、汕头市、佛山市、韶关市、湛江市、肇庆市、江门市、茂名市、惠州市、梅州市、汕尾市、河源市、阳江市、清远市、东莞市、中山市、潮州市、揭阳市、云浮市
		cityLoc.put("广州市", new double[] {113.30765,23.120049});
		cityLoc.put("深圳市", new double[] {114.025974,22.546054});
		cityLoc.put("珠海市", new double[] {113.562447,22.256915});
		cityLoc.put("汕头市", new double[] {116.72865,23.383908});
		cityLoc.put("佛山市", new double[] {113.134026,23.035095});
		cityLoc.put("韶关市", new double[] {113.594461,24.80296});
		cityLoc.put("湛江市", new double[] {110.365067,21.257463});
		cityLoc.put("肇庆市", new double[] {112.479653,23.078663});
		cityLoc.put("江门市", new double[] {113.078125,22.575117});
		cityLoc.put("茂名市", new double[] {110.931245,21.668226});
		cityLoc.put("惠州市", new double[] {114.410658,23.11354});
		cityLoc.put("梅州市", new double[] {116.126403,24.304571});
		cityLoc.put("汕尾市", new double[] {115.372924,22.778731});
		cityLoc.put("河源市", new double[] {114.713721,23.757251});
		cityLoc.put("阳江市", new double[] {111.97701,21.871517});
		cityLoc.put("清远市", new double[] {113.040773,23.698469});
		cityLoc.put("东莞市", new double[] {113.763434,23.043024});
		cityLoc.put("中山市", new double[] {113.42206,22.545178});
		cityLoc.put("潮州市", new double[] {116.630076,23.661812});
		cityLoc.put("揭阳市", new double[] {116.379501,23.547999});
		cityLoc.put("云浮市", new double[] {112.050946,22.937976});
		//海南省共有4个地级市
		//4个地级市：分别是海口市、三亚市、三沙市、儋州市
		cityLoc.put("海口市", new double[] {110.330802,20.022071});
		cityLoc.put("三亚市", new double[] {109.522771,18.257776});
		//cityCoor.put("三沙市", new double[] {109.522771,18.257776});
		cityLoc.put("三沙市", new double[] {109.334586,19.574788});
		//四川省共有1个副省级市、17个地级市、3个自治州
		//1个副省级市: 成都市
		//17个地级市：分别是自贡市、攀枝花市、泸州市、德阳市、绵阳市、广元市、遂宁市、内江市、乐山市、南充市、宜宾市、广安市、达州市、眉山市、雅安市、巴中市、资阳市
		//3个自治州：阿坝藏族羌族自治州、甘孜藏族自治州、凉山彝族自治州
		cityLoc.put("成都市", new double[] {104.067923,30.679943});
		cityLoc.put("自贡市", new double[] {104.776071,29.359157});
		cityLoc.put("攀枝花市", new double[] {101.722423,26.587571});
		cityLoc.put("泸州市", new double[] {105.44397,28.89593});
		cityLoc.put("德阳市", new double[] {104.402398,31.13114});
		cityLoc.put("绵阳市", new double[] {104.705519,31.504701});
		cityLoc.put("广元市", new double[] {105.819687,32.44104});
		cityLoc.put("遂宁市", new double[] {105.564888,30.557491});
		cityLoc.put("内江市", new double[] {105.073056,29.599462});
		cityLoc.put("乐山市", new double[] {103.760824,29.600958});
		cityLoc.put("南充市", new double[] {106.105554,30.800965});
		cityLoc.put("宜宾市", new double[] {104.633019,28.769675});
		cityLoc.put("广安市", new double[] {106.63572,30.463984});
		cityLoc.put("达州市", new double[] {107.494973,31.214199});
		cityLoc.put("眉山市", new double[] {103.84143,30.061115});
		cityLoc.put("雅安市", new double[] {103.009356,29.999716});
		cityLoc.put("巴中市", new double[] {106.757916,31.869189});
		cityLoc.put("资阳市", new double[] {104.63593,30.132191});
		cityLoc.put("阿坝藏族羌族自治州", new double[] {102.228565,31.905763});
		cityLoc.put("甘孜藏族自治州", new double[] {101.969232,30.055144});
		cityLoc.put("凉山彝族自治州", new double[] {102.259591,27.892393});
		//贵州省共有6个地级市、3个自治州
		//6个地级市：分别是贵阳市、遵义市、六盘水市、安顺市、毕节市、铜仁市
		//3个自治州：黔东南苗族侗族自治州、黔南布依族苗族自治州、黔西南布依族苗族自治州
		cityLoc.put("贵阳市", new double[] {106.709177,26.629907});
		cityLoc.put("遵义市", new double[] {106.93126,27.699961});
		cityLoc.put("六盘水市", new double[] {104.852087,26.591866});
		cityLoc.put("安顺市", new double[] {105.92827,26.228595});
		cityLoc.put("毕节市", new double[] {105.333323,27.408562});
		cityLoc.put("铜仁市", new double[] {109.168558,27.674903});
		cityLoc.put("黔东南苗族侗族自治州", new double[] {107.985353,26.583992});
		cityLoc.put("黔南布依族苗族自治州", new double[] {107.523205,26.264536});
		cityLoc.put("黔西南布依族苗族自治州", new double[] {104.900558,25.095148});
		//云南省共有8个地级市、8个少数民族自治州
		// 8个地级市：分别是昆明市，曲靖市，玉溪市，昭通市，保山市，丽江市，普洱市，临沧市
		// 8个少数民族自治州：德宏傣族景颇族自治州 ，怒江傈僳族自治州 ，迪庆藏族自治州 ，大理白族自治州 ，楚雄彝族自治州 ，红河哈尼族彝族自治州 ，文山壮族苗族自治州 ，西双版纳傣族自治州
		cityLoc.put("昆明市", new double[] {102.714601,25.049153});
		cityLoc.put("曲靖市", new double[] {103.782539,25.520758});
		cityLoc.put("玉溪市", new double[] {102.545068,24.370447});
		cityLoc.put("昭通市", new double[] {103.725021,27.340633});
		cityLoc.put("保山市", new double[] {99.177996,25.120489});
		cityLoc.put("丽江市", new double[] {100.229628,26.875351});
		cityLoc.put("普洱市", new double[] {100.980058,22.788778});
		cityLoc.put("临沧市", new double[] {100.092613,23.887806});
		cityLoc.put("德宏傣族景颇族自治州", new double[] {98.589434,24.44124});
		cityLoc.put("怒江傈僳族自治州", new double[] {98.859932,25.860677});
		cityLoc.put("迪庆藏族自治州", new double[] {99.713682,27.831029});
		cityLoc.put("大理白族自治州", new double[] {100.223675,25.5969});
		cityLoc.put("楚雄彝族自治州", new double[] {101.529382,25.066356});
		cityLoc.put("红河哈尼族彝族自治州", new double[] {103.384065,23.367718});
		cityLoc.put("文山壮族苗族自治州", new double[] {104.246294,23.374087});
		cityLoc.put("西双版纳傣族自治州", new double[] {100.803038,22.009433});
		//陕西省共有10个地级市
		// 10个地级市：分别是西安市、咸阳市、宝鸡市、渭南市、铜川市、延安市、榆林市、汉中市、安康市、商洛市
		cityLoc.put("西安市", new double[] {108.953098,34.2778});
		cityLoc.put("咸阳市", new double[] {108.707509,34.345373});
		cityLoc.put("宝鸡市", new double[] {107.170645,34.364081});
		cityLoc.put("渭南市", new double[] {109.483933,34.502358});
		cityLoc.put("铜川市", new double[] {108.968067,34.908368});
		cityLoc.put("延安市", new double[] {109.50051,36.60332});
		cityLoc.put("榆林市", new double[] {109.745926,38.279439});
		cityLoc.put("汉中市", new double[] {107.045478,33.081569});
		cityLoc.put("安康市", new double[] {109.038045,32.70437});
		cityLoc.put("商洛市", new double[] {109.934208,33.873907});
		//甘肃省共有12个地级市、2个自治州
		//12个地级市：分别是兰州市、嘉峪关市、金昌市、白银市、天水市、酒泉市、张掖市、武威市、定西市、陇南市、平凉市、庆阳市
		//2个自治州：宁夏回族自治州、甘南藏族自治州
		cityLoc.put("兰州市", new double[] {103.823305,36.064226});
		cityLoc.put("嘉峪关市", new double[] {98.281635,39.802397});
		cityLoc.put("金昌市", new double[] {102.208126,38.516072});
		cityLoc.put("白银市", new double[] {104.171241,36.546682});
		cityLoc.put("天水市", new double[] {105.736932,34.584319});
		cityLoc.put("酒泉市", new double[] {98.508415,39.741474});
		cityLoc.put("张掖市", new double[] {100.459892,38.93932});
		cityLoc.put("武威市", new double[] {102.640147,37.933172});
		cityLoc.put("定西市", new double[] {104.626638,35.586056});
		cityLoc.put("陇南市", new double[] {104.934573,33.39448});
		cityLoc.put("平凉市", new double[] {106.688911,35.55011});
		cityLoc.put("庆阳市", new double[] {107.644227,35.726801});
		cityLoc.put("宁夏回族自治州", new double[] {106.155481,37.321323});
		cityLoc.put("甘南藏族自治州", new double[] {102.917442,34.992211});
		//青海省共有2个地级市、6个自治州
		//2个地级市：分别是西宁市、海东市
		//6个自治州：海北藏族自治州、黄南藏族自治州、海南藏族自治州、果洛藏族自治州、玉树藏族自治州、海西蒙古族藏族自治州
		cityLoc.put("西宁市", new double[] {101.767921,36.640739});
		cityLoc.put("海东市", new double[] {102.376689,36.312743});
		cityLoc.put("海北藏族自治州", new double[] {100.879802,36.960654});
		cityLoc.put("黄南藏族自治州", new double[] {102.0076,35.522852});
		cityLoc.put("海南藏族自治州", new double[] {100.624066,36.284364});
		cityLoc.put("果洛藏族自治州", new double[] {100.223723,34.480485});
		cityLoc.put("玉树藏族自治州", new double[] {97.013316,33.00624});
		cityLoc.put("海西蒙古族藏族自治州", new double[] {97.342625,37.373799});
		//台湾省共有6个直辖市、3个市
		//6个直辖市：分别是台北市、新北市、桃园市、台中市、台南市、高雄市
		// 3个市：基隆市、新竹市、嘉义市
		cityLoc.put("台北市", new double[] {121.520109,25.06303});
		//cityCoor.put("新北市", new double[] {121.520109,25.06303});
		cityLoc.put("桃园市", new double[] {115.826714,39.701923});
		//cityCoor.put("台中市", new double[] {115.826714,39.701923});
		//cityCoor.put("台南市", new double[] {115.826714,39.701923});
		//cityCoor.put("高雄市", new double[] {115.826714,39.701923});
		//cityCoor.put("基隆市", new double[] {115.826714,39.701923});
		cityLoc.put("新竹市", new double[] {125.291098,43.915305});
		//cityCoor.put("嘉义市", new double[] {125.291098,43.915305});
		//内蒙古自治区共有9个地级市、3个盟
		// 9个地级市：分别是呼和浩特市、包头市、乌海市、赤峰市、通辽市、鄂尔多斯市、呼伦贝尔市、巴彦淖尔市、乌兰察布市
		// 3个盟：兴安盟、锡林郭勒盟、阿拉善盟
		cityLoc.put("新竹市", new double[] {111.660351,40.828319});
		cityLoc.put("包头市", new double[] {109.846239,40.647119});
		cityLoc.put("乌海市", new double[] {106.831999,39.683177});
		cityLoc.put("赤峰市", new double[] {118.930761,42.297112});
		cityLoc.put("通辽市", new double[] {122.260363,43.633756});
		cityLoc.put("鄂尔多斯市", new double[] {109.993706,39.81649});
		cityLoc.put("呼伦贝尔市", new double[] {119.760822,49.201636});
		cityLoc.put("巴彦淖尔市", new double[] {107.423807,40.76918});
		cityLoc.put("乌兰察布市", new double[] {113.112846,41.022363});
		cityLoc.put("兴安盟", new double[] {122.048167,46.083757});
		cityLoc.put("锡林郭勒盟", new double[] {116.02734,43.939705});
		cityLoc.put("阿拉善盟", new double[] {105.695683,38.843075});
		//广西壮族自治区共有14个地级市
		//14个地级市：分别是南宁市、柳州市、桂林市、梧州市、北海市、防城港市、钦州市、贵港市、玉林市、百色市、贺州市、河池市、来宾市、崇左市
		cityLoc.put("南宁市", new double[] {108.297234,22.806493});
		cityLoc.put("柳州市", new double[] {109.422402,24.329053});
		cityLoc.put("桂林市", new double[] {110.26092,25.262901});
		cityLoc.put("梧州市", new double[] {111.305472,23.485395});
		cityLoc.put("北海市", new double[] {109.122628,21.472718});
		cityLoc.put("防城港市", new double[] {108.351791,21.617398});
		cityLoc.put("钦州市", new double[] {108.638798,21.97335});
		cityLoc.put("贵港市", new double[] {109.613708,23.103373});
		cityLoc.put("玉林市", new double[] {110.151676,22.643974});
		cityLoc.put("百色市", new double[] {106.631821,23.901512});
		cityLoc.put("贺州市", new double[] {111.552594,24.411054});
		cityLoc.put("河池市", new double[] {108.069948,24.699521});
		cityLoc.put("来宾市", new double[] {109.231817,23.741166});
		cityLoc.put("崇左市", new double[] {107.357322,22.415455});
		//西藏自治区共有6个地级市、1个地区
		// 6个地级市：分别是拉萨市、日喀则市、昌都市、林芝市、山南市、那曲市
		//1个地区：阿里地区
		cityLoc.put("拉萨市", new double[] {91.111891,29.662557});
		cityLoc.put("日喀则市", new double[] {88.956063,29.26816});
		cityLoc.put("昌都市", new double[] {96.36244,30.510925});
		cityLoc.put("林芝市", new double[] {95.466234,29.12808});
		cityLoc.put("山南市", new double[] {92.220873,28.354982});
		cityLoc.put("那曲市", new double[] {92.067018,31.48068});
		cityLoc.put("阿里地区", new double[] {81.107669,30.404557});
		//宁夏回族自治区共有5个地级市
		//5个地级市：分别是银川市、石嘴山市、吴忠市、固原市、中卫市
		cityLoc.put("银川市", new double[] {106.206479,38.502621});
		cityLoc.put("石嘴山市", new double[] {106.379337,39.020223});
		cityLoc.put("吴忠市", new double[] {106.208254,37.993561});
		cityLoc.put("固原市", new double[] {106.285268,36.021523});
		cityLoc.put("中卫市", new double[] {105.196754,37.521124});
		//新疆维吾尔自治区共有4地级市、5地区、5自治州
		//4地级市：分别是乌鲁木齐市、克拉玛依市、吐鲁番市、哈密市
		//5地区：阿克苏地区、喀什地区、和田地区、塔城地区、阿勒泰地区
		//5自治州：昌吉回族自治州、博尔塔拉蒙古自治州、巴音郭楞蒙古自治州、克孜勒苏柯尔克孜自治州、伊犁哈萨克自治州
		cityLoc.put("乌鲁木齐市", new double[] {87.564988,43.84038});
		cityLoc.put("克拉玛依市", new double[] {84.88118,45.594331});
		cityLoc.put("吐鲁番市", new double[] {89.266025,42.678925});
		cityLoc.put("哈密市", new double[] {93.529373,42.344467});
		cityLoc.put("阿克苏地区", new double[] {80.269846,41.171731});
		cityLoc.put("喀什地区", new double[] {75.992973,39.470627});
		cityLoc.put("和田地区", new double[] {79.930239,37.116774});
		cityLoc.put("塔城地区", new double[] {82.974881,46.758684});
		cityLoc.put("阿勒泰地区", new double[] {88.137915,47.839744});
		cityLoc.put("昌吉回族自治州", new double[] {87.296038,44.007058});
		cityLoc.put("博尔塔拉蒙古自治州", new double[] {82.052436,44.913651});
		cityLoc.put("巴音郭楞蒙古自治州", new double[] {86.121688,41.771362});
		cityLoc.put("克孜勒苏柯尔克孜自治州", new double[] {76.137564,39.750346});
		cityLoc.put("伊犁哈萨克自治州", new double[] {81.297854,43.922248});
	}
	
	/**
	 * 获取某个中国城市的经纬度
	 * @param city 城市名
	 * @return 返回该城市的坐标
	 */
	public double[] getCityLoc(String city) {
		double[] dou = new double[2];
		if(this.isChineseCity(city)) {
			dou = cityLoc.get(city);
		}
		return dou;
	}
	
}
