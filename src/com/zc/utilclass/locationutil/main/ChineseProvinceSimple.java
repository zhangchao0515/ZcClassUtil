package com.zc.utilclass.locationutil.main;

import java.util.HashMap;
import java.util.Map;

/**
 * 中国省份的相关信息
 * 
 * @author zhangchao
 * @date 2018-09-10
 */
public class ChineseProvinceSimple {
	// String province; // 省份名称
	// 省份坐标（经纬度），以省会坐标为自己省的坐标
	// Coordinate coordinate = new Coordinate();
	// 省会城市
	// String provincialcapital;
	// 该省包含的城市列表
	// String city[];

	// String 代表省份，Coordinate代表该省份（省会的位置）的经纬度，
	Map<String, double[]> provinceCoor = new HashMap<String,double[]>();

	/**
	 * 省份指中国的行政规划，我国共有34个省级行政区域，包括23个省，5个自治区，4个直辖市，2个特别行政区。 包括：
	 * 23个省:河北省，山西省，辽宁省，吉林省，黑龙江省，江苏省，浙江省，安徽省，福建省，江西省，山东省，河南省，湖北省，湖南省，广东省，海南省，四川省，贵州省，云南省，陕西省，甘肃省，青海省，台湾省，
	 * 5个自治区:内蒙古自治区，广西壮族自治区，西藏自治区，宁夏回族自治区，新疆维吾尔自治区， 
	 * 4个直辖市:北京市，天津市，上海市，重庆市， 
	 * 2个特别行政区:香港特别行政区，澳门特别行政区。
	 */
	// 23个省
	final String province[] = { "河北省", "山西省", "辽宁省", "吉林省", "黑龙江省", "江苏省", "浙江省", "安徽省", "福建省", "江西省", "山东省", "河南省",
			"湖北省", "湖南省", "广东省", "海南省", "四川省", "贵州省", "云南省", "陕西省", "甘肃省", "青海省", "台湾省" };
	// 5个自治区
	final String autonomousRegion[] = { "内蒙古自治区", "广西壮族自治区", "西藏自治区", "宁夏回族自治区", "新疆维吾尔自治区" };
	// 4个直辖市
	final String municipality[] = { "北京市", "天津市", "上海市", "重庆市" };
	// 2个特别行政区
	final String specialAdministrativeRegion[] = { "香港特别行政区", "澳门特别行政区" };

	// 所有的34个省级行政区域
	final String chineseProvince[] = { "河北省", "山西省", "辽宁省", "吉林省", "黑龙江省", "江苏省", "浙江省", "安徽省", "福建省", "江西省", "山东省",
			"河南省", "湖北省", "湖南省", "广东省", "海南省", "四川省", "贵州省", "云南省", "陕西省", "甘肃省", "青海省", "台湾省", "内蒙古自治区", "广西壮族自治区",
			"西藏自治区", "宁夏回族自治区", "新疆维吾尔自治区", "北京市", "天津市", "上海市", "重庆市", "香港特别行政区", "澳门特别行政区" };
	// 所有的34个省级行政区域简称
	final String chineseProvinceShort[] = { "冀", "晋", "辽", "吉", "黑", "苏", "浙", "皖", "闽", "赣", "鲁", "豫", "鄂", "湘", "粤",
			"琼", "川", "贵", "云", "陕", "甘", "青", "台", "蒙", "桂", "藏", "宁", "新", "京", "津", "沪", "渝", "港", "澳" };

	/**
	 * 所有的34个省级行政区域及简称
	 * "河北省（冀）","山西省（晋）","辽宁省（辽）","吉林省（吉）","黑龙江省（黑）","江苏省（苏）","浙江省（浙）","安徽省（皖）","福建省（闽）","江西省（赣）","山东省（鲁）","河南省（豫）","湖北省（鄂）","湖南省（湘）","广东省（粤）","海南省（琼）","四川省（川）","贵州省（贵）","云南省（云）","陕西省（陕）","甘肃省（甘）","青海省（青）","台湾省（台）",
	 * "内蒙古自治区（蒙）","广西壮族自治区（桂）","西藏自治区（藏）","宁夏回族自治区（宁）","新疆维吾尔自治区（新）",
	 * "北京市（京）","天津市（津）","上海市（沪）","重庆市（渝）", "香港特别行政区（港）","澳门特别行政区（澳）"
	 */
	final String chineseProvinceAndShort[] = { "河北省（冀）", "山西省（晋）", "辽宁省（辽）", "吉林省（吉）", "黑龙江省（黑）", "江苏省（苏）", "浙江省（浙）",
			"安徽省（皖）", "福建省（闽）", "江西省（赣）", "山东省（鲁）", "河南省（豫）", "湖北省（鄂）", "湖南省（湘）", "广东省（粤）", "海南省（琼）", "四川省（川）",
			"贵州省（贵）", "云南省（云）", "陕西省（陕）", "甘肃省（甘）", "青海省（青）", "台湾省（台）", "内蒙古自治区（蒙）", "广西壮族自治区（桂）", "西藏自治区（藏）",
			"宁夏回族自治区（宁）", "新疆维吾尔自治区（新）", "北京市（京）", "天津市（津）", "上海市（沪）", "重庆市（渝）", "香港特别行政区（港）", "澳门特别行政区（澳）" };

	// 所有的34个省级行政区域的省会城市 "青海省（青）","台湾省（台）
	final String provincialCapital[] = { "石家庄市", "太原市", "沈阳市", "长春市", "哈尔滨市", "南京市", "杭州市", "合肥市", "福州市", "南昌市", "济南市",
			"郑州市", "武汉市", "长沙市", "广州市", "海口市", "成都市", "贵阳市", "昆明市", "西安市", "兰州市", "西宁市", "台北市", "呼和浩特市", "南宁市", "拉萨市",
			"银川市", "乌鲁木齐市", "北京市", "天津市", "上海市", "重庆市", "香港", "澳门" };

	public ChineseProvinceSimple() {
		// 给各省份添加经纬度
		setProvinceCoor();
	}

	public String[] getProvince() {
		return province;
	}

	public String[] getAutonomousRegion() {
		return autonomousRegion;
	}

	public String[] getMunicipality() {
		return municipality;
	}

	public String[] getSpecialAdministrativeRegion() {
		return specialAdministrativeRegion;
	}

	public String[] getChineseProvince() {
		return chineseProvince;
	}

	public String[] getChineseProvinceShort() {
		return chineseProvinceShort;
	}

	public String[] getChineseProvinceAndShort() {
		return chineseProvinceAndShort;
	}

	public String[] getProvincialCapital() {
		return provincialCapital;
	}

	public Map<String, double[]> getProvinceCoor() {
		return provinceCoor;
	}

	//public void setProvinceCoor(Map<String, Double[]> provinceCoor) {
		//this.provinceCoor = provinceCoor;
	//}

	/**
	 * 设置中国所有省份（省会城市）的经纬度
	 */
	public void setProvinceCoor() {
		double coordinate[] = new double[2];
		// 23个省，5个自治区，4个直辖市，2个特别行政区
		// 23个省
		// 河北省石家庄市
		provinceCoor.put("河北省", new double[]{114.522082,38.048958});
		// 山西省太原市
		provinceCoor.put("山西省", new double[]{112.550864,37.890277});
		// 辽宁省沈阳市
		provinceCoor.put("辽宁省", new double[]{123.432791,41.808645});
		// 吉林省长春市
		provinceCoor.put("吉林省", new double[]{125.313642,43.898338});
		// 黑龙江省哈尔滨市
		provinceCoor.put("黑龙江省", new double[]{126.657717,45.773225});
		// 江苏省南京市
		provinceCoor.put("江苏省", new double[]{118.778074,32.057236});
		// 浙江省杭州市
		provinceCoor.put("浙江省", new double[]{120.219375,30.259244});
		// 安徽省合肥市
		provinceCoor.put("安徽省", new double[]{117.282699,31.866942});
		// 福建省福州市
		provinceCoor.put("福建省", new double[]{119.330221,26.047125});
		// 江西省南昌市
		provinceCoor.put("江西省", new double[]{115.893528,28.689578});
		// 山东省济南市
		provinceCoor.put("山东省", new double[]{117.024967,36.682785});
		// 河南省郑州市
		provinceCoor.put("河南省", new double[]{113.649644,34.75661});
		// 湖北省武汉市
		provinceCoor.put("湖北省", new double[]{114.3162,30.581084});
		// 湖南省长沙市
		provinceCoor.put("湖南省", new double[]{112.979353,28.213478});
		// 广东省广州市
		provinceCoor.put("广东省", new double[]{113.30765,23.120049});
		// 海南省海口市
		provinceCoor.put("海南省", new double[]{110.330802,20.022071});
		// 四川省成都市
		provinceCoor.put("四川省", new double[]{104.067923,30.679943});
		// 贵州省贵阳市
		provinceCoor.put("贵州省", new double[]{106.709177,26.629907});
		// 云南省昆明市
		provinceCoor.put("云南省", new double[]{102.714601,25.049153});
		// 陕西省西安市
		provinceCoor.put("陕西省", new double[]{108.953098,34.2778});
		// 甘肃省兰州市
		provinceCoor.put("甘肃省", new double[]{103.823305,36.064226});
		// 青海省西宁市
		provinceCoor.put("青海省", new double[]{101.767921,36.640739});
		// 台湾省台北市
		provinceCoor.put("台湾省", new double[]{121.520109,25.06303});
		
		// 5个自治区
		// 内蒙古自治区呼和浩特市
		provinceCoor.put("内蒙古自治区", new double[]{111.660351,40.828319});
		// 广西壮族自治区南宁市
		provinceCoor.put("广西壮族自治区", new double[]{108.297234,22.806493});
		// 西藏自治区拉萨市
		provinceCoor.put("西藏自治区", new double[]{91.111891,29.662557});
		// 宁夏回族自治区银川市
		provinceCoor.put("宁夏回族自治区", new double[]{106.206479,38.502621});
		// 新疆维吾尔自治区乌鲁木齐市
		provinceCoor.put("新疆维吾尔自治区", new double[]{87.564988,43.84038});
		
		// 4个直辖市
		// 北京市
		provinceCoor.put("北京市", new double[]{116.395645,39.929986});
		// 天津市
		provinceCoor.put("天津市", new double[]{117.210813,39.14393});
		// 上海市
		provinceCoor.put("上海市", new double[]{121.487899,31.249162});
		// 重庆市
		provinceCoor.put("重庆市", new double[]{106.530635,29.544606});
		
		// 2个特别行政区
		// 香港特别行政区
		provinceCoor.put("香港特别行政区", new double[]{114.186124,22.293586});
		// 澳门特别行政区
		provinceCoor.put("澳门特别行政区", new double[]{113.557519,22.204118});
	}

	/**
	 * 获取中国某个省份的名称
	 * 
	 * @param province
	 * @return
	 */
	public double[] getProvinceName(String province) {
		double[] coordinate = new double[2];
		
		if(isChineseProvince(province)) {
			//
			province = standardProvince(province);
			coordinate = provinceCoor.get(province);
		}else {
			throw new IllegalArgumentException("省份错误或者不是中国省份");
		}
		return coordinate;
	}
	
	/**
	 * 获取中国某个省份的经纬度
	 * 
	 * @param province
	 * @return
	 */
	public double[] getProvinceCoor(String province) {
		double[] coordinate = new double[2];
		
		if(isChineseProvince(province)) {
			//
			province = standardProvince(province);
			coordinate = provinceCoor.get(province);
		}else {
			throw new IllegalArgumentException("省份错误或者不是中国省份");
		}
		return coordinate;
	}

	/**
	 * 判断是否是中国省份
	 * 
	 * @param province
	 * @return
	 */
	public boolean isChineseProvince(String province) {
		province = standardProvince(province);
		// String str = "";
		for (String str : chineseProvince) {
			if (str.equals(province))
				return true;
		}
		// throw new IllegalArgumentException("省份错误或者不是中国省份");
		return false;
	}

	/**
	 * 规范省名, 全称
	 * 
	 * @param province
	 * 省名 省份指中国的行政规划，我国共有34个省级行政区域，包括23个省，5个自治区，4个直辖市，2个特别行政区。 包括：
	 * 河北省，山西省，辽宁省，吉林省，黑龙江省，江苏省，浙江省，安徽省，福建省，江西省，山东省，河南省，湖北省，湖南省，广东省，海南省，四川省，贵州省，云南省，陕西省，甘肃省，青海省，台湾省，
	 * 内蒙古自治区，广西壮族自治区，西藏自治区，宁夏回族自治区，新疆维吾尔自治区， 北京市，天津市，上海市，重庆市，
	 * 香港特别行政区，澳门特别行政区。
	 */
	private String standardProvince(String province) {
		switch (province) {
		// 23个省
		case "河北省":
		case "河北":
			province = "河北省";
			break;
		case "山西省":
		case "山西":
			province = "山西省";
			break;
		case "辽宁省":
		case "辽宁":
			province = "辽宁省";
			break;
		case "吉林省":
		case "吉林":
			province = "吉林省";
			break;
		case "黑龙江省":
		case "黑龙江":
			province = "黑龙江省";
			break;
		case "江苏省":
		case "江苏":
			province = "江苏省";
			break;
		case "浙江省":
		case "浙江":
			province = "浙江省";
			break;
		case "安徽省":
		case "安徽":
			province = "安徽省";
			break;
		case "福建省":
		case "福建":
			province = "福建省";
			break;
		case "江西省":
		case "江西":
			province = "江西省";
			break;
		case "山东省":
		case "山东":
			province = "山东省";
			break;
		case "河南省":
		case "河南":
			province = "河南省";
			break;
		case "湖北省":
		case "湖北":
			province = "湖北省";
			break;
		case "湖南省":
		case "湖南":
			province = "湖南省";
			break;
		case "广东省":
		case "广东":
			province = "广东省";
			break;
		case "海南省":
		case "海南":
			province = "海南省";
			break;
		case "四川省":
		case "四川":
			province = "四川省";
			break;
		case "贵州省":
		case "贵州":
			province = "贵州省";
			break;
		case "云南省":
		case "云南":
			province = "云南省";
			break;
		case "陕西省":
		case "陕西":
			province = "陕西省";
			break;
		case "甘肃省":
		case "甘肃":
			province = "甘肃省";
			break;
		case "青海省":
		case "青海":
			province = "青海省";
			break;
		case "台湾省":
		case "台湾":
			province = "台湾省";
			break;
		// 5个自治区
		case "内蒙古自治区":
		case "内蒙古":
			province = "内蒙古自治区";
			break;
		case "广西":
		case "广西壮族":
		case "广西壮族自治区":
			province = "广西壮族自治区";
			break;
		case "西藏":
		case "西藏自治区":
			province = "西藏自治区";
			break;
		case "宁夏":
		case "宁夏回族":
		case "宁夏回族自治区":
			province = "宁夏回族自治区";
			break;
		case "新疆":
		case "新疆维吾尔":
		case "新疆维吾尔自治区":
			province = "新疆维吾尔自治区";
			break;
		// 4个直辖市
		case "北京市":
		case "北京":
			province = "北京市";
			break;
		case "天津市":
		case "天津":
			province = "天津市";
			break;
		case "上海市":
		case "上海":
			province = "上海市";
			break;
		case "重庆市":
		case "重庆":
			province = "重庆市";
			break;
		// 2个特别行政区
		case "香港特别行政区":
		case "香港":
			province = "香港特别行政区";
			break;
		case "澳门特别行政区":
		case "澳门":
			province = "澳门特别行政区";
			break;
		default:
			System.out.println("输入省份不对！");
			break;
		}
		return province;
	}

	/**
	 * 规范省名, 简写
	 * 
	 * @param province
	 *            省名 省份指中国的行政规划，我国共有34个省级行政区域，包括23个省，5个自治区，4个直辖市，2个特别行政区。 包括：
	 *            河北省，山西省，辽宁省，吉林省，黑龙江省，江苏省，浙江省，安徽省，福建省，江西省，山东省，河南省，湖北省，湖南省，广东省，海南省，四川省，贵州省，云南省，陕西省，甘肃省，青海省，台湾省，
	 *            内蒙古自治区，广西壮族自治区，西藏自治区，宁夏回族自治区，新疆维吾尔自治区， 北京市，天津市，上海市，重庆市，
	 *            香港特别行政区，澳门特别行政区。
	 */
	private String standardProvinceShort(String province) {
		switch (province) {
		// 23个省
		case "河北省":
		case "河北":
			province = "河北";
			break;
		case "山西省":
		case "山西":
			province = "山西";
			break;
		case "辽宁省":
		case "辽宁":
			province = "辽宁";
			break;
		case "吉林省":
		case "吉林":
			province = "吉林";
			break;
		case "黑龙江省":
		case "黑龙江":
			province = "黑龙江";
			break;
		case "江苏省":
		case "江苏":
			province = "江苏";
			break;
		case "浙江省":
		case "浙江":
			province = "浙江";
			break;
		case "安徽省":
		case "安徽":
			province = "安徽";
			break;
		case "福建省":
		case "福建":
			province = "福建";
			break;
		case "江西省":
		case "江西":
			province = "江西";
			break;
		case "山东省":
		case "山东":
			province = "山东";
			break;
		case "河南省":
		case "河南":
			province = "河南";
			break;
		case "湖北省":
		case "湖北":
			province = "湖北";
			break;
		case "湖南省":
		case "湖南":
			province = "湖南";
			break;
		case "广东省":
		case "广东":
			province = "广东";
			break;
		case "海南省":
		case "海南":
			province = "海南";
			break;
		case "四川省":
		case "四川":
			province = "四川";
			break;
		case "贵州省":
		case "贵州":
			province = "贵州";
			break;
		case "云南省":
		case "云南":
			province = "云南";
			break;
		case "陕西省":
		case "陕西":
			province = "陕西";
			break;
		case "甘肃省":
		case "甘肃":
			province = "甘肃";
			break;
		case "青海省":
		case "青海":
			province = "青海";
			break;
		case "台湾省":
		case "台湾":
			province = "台湾";
			break;
		// 5个自治区
		case "内蒙古自治区":
		case "内蒙古":
			province = "内蒙古";
			break;
		case "广西":
		case "广西壮族":
		case "广西壮族自治区":
			province = "广西";
			break;
		case "西藏":
		case "西藏自治区":
			province = "西藏";
			break;
		case "宁夏":
		case "宁夏回族":
		case "宁夏回族自治区":
			province = "宁夏";
			break;
		case "新疆":
		case "新疆维吾尔":
		case "新疆维吾尔自治区":
			province = "新疆";
			break;
		// 4个直辖市
		case "北京市":
		case "北京":
			province = "北京";
			break;
		case "天津市":
		case "天津":
			province = "天津";
			break;
		case "上海市":
		case "上海":
			province = "上海";
			break;
		case "重庆市":
		case "重庆":
			province = "重庆";
			break;
		// 2个特别行政区
		case "香港特别行政区":
		case "香港":
			province = "香港";
			break;
		case "澳门特别行政区":
		case "澳门":
			province = "澳门";
			break;
		default:
			System.out.println("输入省份不对！");
			break;
		}
		return province;
	}

	/**
	 * @param province
	 * @return 省会简称 省份指中国的行政规划，我国共有34个省级行政区域，包括23个省，5个自治区，4个直辖市，2个特别行政区。 包括：
	 *         河北省（冀），山西省（晋），辽宁省（辽），吉林省（吉），黑龙江省（黑），江苏省（苏），浙江省（浙），安徽省（皖），福建省（闽），江西省（赣），山东省（鲁），河南省（豫），湖北省（鄂），湖南省（湘），广东省（粤），海南省（琼），四川省（川），贵州省（贵），云南省（云），陕西省（陕），甘肃省（甘），青海省（青），台湾省(台)，
	 *         内蒙古自治区（蒙），广西壮族自治区（桂），西藏自治区（藏），宁夏回族自治区（宁），新疆维吾尔自治区（新），
	 *         北京市（京），天津市（津），上海市（沪），重庆市（渝）， 香港特别行政区（港），澳门特别行政区（澳）。
	 */
	public String getProvinceShort2(String province) {
		province = standardProvince(province);
		switch (province) {
		// 23个省粤
		case "河北省":
			province = "冀";
			break;
		case "山西省":
			province = "晋";
			break;
		case "辽宁省":
			province = "辽";
			break;
		case "吉林省":
			province = "吉";
			break;
		case "贵州":
		case "黑龙江省":
			province = "黑";
			break;
		case "江苏省":
			province = "苏";
			break;
		case "浙江省":
			province = "浙";
			break;
		case "安徽省":
			province = "皖";
			break;
		case "福建省":
			province = "闽";
			break;
		case "江西省":
			province = "赣";
			break;
		case "山东省":
			province = "鲁";
			break;
		case "河南省":
			province = "豫";
			break;
		case "湖北省":
			province = "鄂";
			break;
		case "湖南省":
			province = "湘";
			break;
		case "广东省":
			province = "粤";
			break;
		case "海南省":
			province = "琼";
			break;
		case "四川省":
			province = "川";
			break;
		case "贵州省":
			province = "贵";
			break;
		case "云南省":
			province = "云";
			break;
		case "陕西省":
			province = "陕";
			break;
		case "甘肃省":
			province = "甘";
			break;
		case "青海省":
			province = "青";
			break;
		case "台湾省":
			province = "台";
			break;
		// 5个自治区
		case "内蒙古自治区":
			province = "蒙";
			break;
		case "广西壮族自治区":
			province = "桂";
			break;
		case "西藏自治区":
			province = "藏";
			break;
		case "宁夏回族自治区":
			province = "宁";
			break;
		case "新疆维吾尔自治区":
			province = "新";
			break;
		// 4个直辖市
		case "北京市":
			province = "京";
			break;
		case "天津市":
			province = "津";
			break;
		case "上海市":
			province = "沪";
			break;
		case "重庆市":
			province = "渝";
			break;
		// 2个特别行政区
		case "香港特别行政区":
			province = "港";
			break;
		case "澳门特别行政区":
			province = "澳";
			break;
		default:
			System.out.println("输入省份不对！");
			break;
		}
		return province;
	}

	/**
	 * @param province
	 * @return 省会简称 省份指中国的行政规划，我国共有34个省级行政区域，包括23个省，5个自治区，4个直辖市，2个特别行政区。 包括：
	 *         河北省（冀），山西省（晋），辽宁省（辽），吉林省（吉），黑龙江省（黑），江苏省（苏），浙江省（浙），安徽省（皖），福建省（闽），江西省（赣），山东省（鲁），河南省（豫），湖北省（鄂），湖南省（湘），广东省（粤），海南省（琼），四川省（川），贵州省（贵），云南省（云），陕西省（陕），甘肃省（甘），青海省（青），台湾省(台)，
	 *         内蒙古自治区（蒙），广西壮族自治区（桂），西藏自治区（藏），宁夏回族自治区（宁），新疆维吾尔自治区（新），
	 *         北京市（京），天津市（津），上海市（沪），重庆市（渝）， 香港特别行政区（港），澳门特别行政区（澳）。
	 */
	public String getProvinceShort(String province) {
		int loc = getProvinceLoc(province);
		return chineseProvinceShort[loc];
	}

	/**
	 * 返回该省份在 chineseProvince 数组中的坐标
	 * 
	 * @param province
	 *            省份
	 * @return
	 */
	private int getProvinceLoc(String province) {
		province = standardProvince(province);
		int loc = 0;
		if (isChineseProvince(province)) {
			for (int i = 0; i < chineseProvince.length; i++) {
				if (province.equals(chineseProvince[i])) {
					loc = i;
				}
			}
		} else {
			throw new IllegalArgumentException("省份错误或者不是中国省份");
		}
		return loc;
	}

}
