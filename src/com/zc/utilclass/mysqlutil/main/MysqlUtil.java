package com.zc.utilclass.mysqlutil.main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * java连接MySQL数据库，并获取数据库中的数据
 * @author zhangchao
 * @date 2018-8-2 13:47
 */
public class MysqlUtil {

	private Connection con; //声明Connection对象
	private ResultSet rs; //ResultSet类，用来存放获取的结果集！！
	private String ip; //要连接的主机IP，本地连接localhost
	private int port;  //连接的端口号，默认3306
	private String user; // MySQL配置时的用户名
	private String password; //MySQL配置时的密码
	private String database; //要连接的数据库名称
	private String table; //表名
	private String sql; //要执行的SQL语句
	
	public MysqlUtil() {
		
	}
	
	public ResultSet getRs() {
		return rs;
	}

	public void setRs(ResultSet rs) {
		this.rs = rs;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	/*
	 * java连接MySQL，并获取数据，建立连接后，最后一定要调用 释放连接：
	 * 1.工具：eclipse, MySQL5.6, MySQL连接驱动：mysql-connector-java-5.1.27.jar
	 * 2.加载驱动：　1)在工程目录中创建lib文件夹，将下载好的JDBC放到该文件夹下;
	 *    2)右键工程名，在java build path中的Libraries分页中选择Add JARs...，选择刚才添加的JDBC。
	 * 3. 知道MySQL登录名和密码；数据库以及表。
	 * @author zhangchao
	 * @date 2018-8-2 15:55
	 * @param user:MySQL配置时的用户名
	 * @param password:MySQL配置时的密码
	 * @param database:要连接的数据库名称
	 */
	public void mysqlConnectJava(String user, String password, String database) {
		//没有ip，使用本地连接localhost；没有端口号，使用默认端口号3306
		this.mysqlConnectJava("localhost",3306,user, password,database);
	}
	
	/* @date 2018-8-2 15:50
	 * @param ip:要连接的主机IP，本地连接localhost
	 * @param user:MySQL配置时的用户名
	 * @param password:MySQL配置时的密码
	 * @param database:要连接的数据库名称
	 */
	public void mysqlConnectJava(String ip,String user, String password, String database) {
		//没有端口号，使用默认端口号3306
		this.mysqlConnectJava(ip,3306,user, password,database);
	}	
	
	/* @date 2018-8-2 15:00
	 * @param ip:要连接的主机IP，本地连接localhost
	 * @param user:MySQL配置时的用户名
	 * @param password:MySQL配置时的密码
	 * @param database:要连接的数据库名称
	 */
	public void mysqlConnectJava(int port,String user, String password, String database) {
		//没有端口号，使用默认端口号3306
		this.mysqlConnectJava("localhost",port,user, password,database);
	}
	
	/* 
	 * @date 2018-8-2 14:30
	 * @param ip:要连接的主机IP，本地连接localhost
	 * @param port:连接的端口号，默认3306
	 * @param user:MySQL配置时的用户名
	 * @param password:MySQL配置时的密码
	 * @param database:要连接的数据库名称
	 */
	public void mysqlConnectJava(String ip,int port,String user, String password, String database) {
		this.setIp(ip);
		this.setPort(port);
		this.setUser(user);
		this.setPassword(password);
		this.setDatabase(database);
		//驱动程序名
		String driver = "com.mysql.jdbc.Driver";
		//URL指向要访问的数据库名
		String url = "jdbc:mysql://" + ip + ":" + String.valueOf(port) + "/" + database;
		//System.out.println(url);
		
		//遍历查询结果集
		try {
		    //加载驱动程序
			Class.forName(driver);
			//1.getConnection()方法，连接MySQL数据库！！
			con = DriverManager.getConnection(url,user,password);
			if(!con.isClosed())
				System.out.println("Succeeded connecting to the Database!");
			/*内容*/
		} catch(ClassNotFoundException e) {   
			//数据库驱动类异常处理
			System.out.println("Sorry,can`t find the Driver!");   
			e.printStackTrace();   
		} catch(SQLException e) {
			//数据库连接失败异常处理
			e.printStackTrace();  
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			System.out.println("成功建立数据库\""+ this.getDatabase() +"\"连接！！");
		}
	}
	
	/* 
	 * Java关闭MySQL数据库连接
	 * @date 2018-8-2 15:40
	 */
	public void mysqlClosedConnectJava() {
		try {
			con.close();
		} catch (SQLException e) {
			// 数据库连接失败异常处理
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			System.out.println("成功关闭数据库\"" + this.getDatabase() + "\"连接！！");
		}
	}
	
	/* @date 2018-8-2 15:30
	 * 连接数据库中的表
	 * @param table: 表名
	 * @param sql: 要执行的SQL语句，其中employee是mysqltest数据库中的表
	 */
	public void connectTable(String table, String sql) {
		this.setTable(table);
		this.setSql(sql);
		//2.创建statement类对象，用来执行SQL语句！！
		Statement statement;
		//Object obj = new Object();
		try {
			statement = con.createStatement();
		
			//要执行的SQL语句，其中employee是mysqltest数据库中的表
			//String sql = "select * from " + table;
			//System.out.println(sql);
			
			//3.ResultSet类，用来存放获取的结果集！！
			rs = statement.executeQuery(sql);
			this.setRs(rs);
		} catch (SQLException e) {
			// 数据库连接失败异常处理
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			System.out.println("成功连接表\"" + this.getTable() +"\"！！");
		}
	}
	
	/* @date 2018-8-5 20:25
	 * 最后关闭表连接
	 * @param table: 表名
	 * @param sql: 要执行的SQL语句，其中employee是mysqltest数据库中的表
	 */
	public void closedTableConnect() {
		try {
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			System.out.println("成功关闭表\"" + this.getTable() + "\"连接！！");
		}
	}
	
	/*
	 * 获取表数据，对表数据进行处理
	 * @date 2018-8-2 16:50
	 */
	public Object processTableData(ResultSet rs) {
		// Demo
		String str = processTableDataDemo(rs);
		//return str;
		return str;
	}
	
	/*
	 * processTableData处理的一个Demo
	 * @date 2018-08-02 17:02
	 */
	private String processTableDataDemo(ResultSet rs) {
		// 显示表中数据
		String str = "";
		//System.out.println("-----------------");
		//System.out.println("执行结果如下所示:");  
		//System.out.println("-----------------");  
		//System.out.println("姓名" + "\t" + "职称");  
		//System.out.println("-----------------");  
		str += "-----------------\n";
		str += "执行结果如下所示:\n";
		str += "-----------------\n";
		str += "姓名" + "\t" + "职称\n";
		str += "-----------------\n";
				
		String job = null;
		String id = null;

		try {
			while(rs.next()){
				//获取stuname这列数据
				job = rs.getString("job");
				//获取stuid这列数据
				id = rs.getString("ename");

				//输出结果
				//System.out.println(id + "\t" + job);
				str += id + "\t" + job + "\n";
			}
		} catch (SQLException e) {
			// 数据库连接失败异常处理
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return str;
	}

}
