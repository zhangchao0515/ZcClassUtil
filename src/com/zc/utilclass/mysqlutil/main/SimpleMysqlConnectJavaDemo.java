package com.zc.utilclass.mysqlutil.main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * java连接MySQL数据库，并获取数据库中的数据
 * @author zhangchao
 * @date 2018-8-2 13:47
 */
public class SimpleMysqlConnectJavaDemo {

	/*
	 * java连接MySQL，并获取数据：
	 * 1.工具：eclipse, MySQL5.6, MySQL连接驱动：mysql-connector-java-5.1.27.jar
	 * 2.加载驱动：　1)在工程目录中创建lib文件夹，将下载好的JDBC放到该文件夹下;
	 *    2)右键工程名，在java build path中的Libraries分页中选择Add JARs...，选择刚才添加的JDBC。
	 * 3. 知道MySQL登录名和密码；数据库以及表。
	 */
	public static void main(String[] args) {
		//声明Connection对象
		Connection con;
		//驱动程序名
		String driver = "com.mysql.jdbc.Driver";
		//URL指向要访问的数据库名， 这里我本地的数据库名mysqltest
		String url = "jdbc:mysql://localhost:3306/mysqltest";
		//MySQL配置时的用户名
		String user = "root";
		//MySQL配置时的密码
		String password = "0515";
		//遍历查询结果集
		try {
		    //加载驱动程序
			Class.forName(driver);
			//1.getConnection()方法，连接MySQL数据库！！
			con = DriverManager.getConnection(url,user,password);
			if(!con.isClosed())
				System.out.println("Succeeded connecting to the Database!");
			//2.创建statement类对象，用来执行SQL语句！！
			Statement statement = con.createStatement();
			//要执行的SQL语句，其中employee是mysqltest数据库中的表
			String sql = "select * from employee";
			//3.ResultSet类，用来存放获取的结果集！！
			ResultSet rs = statement.executeQuery(sql);
			
			// 显示表中数据
			System.out.println("-----------------");
			System.out.println("执行结果如下所示:");  
			System.out.println("-----------------");  
			System.out.println("姓名" + "\t" + "职称");  
			System.out.println("-----------------");  
      
			String job = null;
			String id = null;
			while(rs.next()){
				//获取stuname这列数据
				job = rs.getString("job");
				//获取stuid这列数据
				id = rs.getString("ename");

				//输出结果
				System.out.println(id + "\t" + job);
			}
			rs.close();
			con.close();
		} catch(ClassNotFoundException e) {   
			//数据库驱动类异常处理
			System.out.println("Sorry,can`t find the Driver!");   
			e.printStackTrace();   
		} catch(SQLException e) {
			//数据库连接失败异常处理
			e.printStackTrace();  
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			System.out.println("数据库数据成功获取！！");
		}
	}
}
