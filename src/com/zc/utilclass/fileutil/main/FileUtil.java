package com.zc.utilclass.fileutil.main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;  // 如果有数组

/**
 * @author zhangchao 
 * 自己封装的文件（File）工具类
 */
public class FileUtil {
	  /**
	   * 向指定的文件中写入内容;  
	   * 这里是追加写, 想重新写,先删除生成的文件, 或者把文件删除代码注释取消
	   * @author zhangchao
	   * @version 2018年8月23号 
	   * @param filecontent, 要写入文件的内容 String 或 Object
	   */
	  public void writeToFile(Object filecontent){
	    String path = "/home/zhangchao/vscode/";
	    String filename = "MyTest.txt";
	    String filenameTemp = path + filename;
	    String filein = filecontent + "\r\n";  //新写入的行，换行
	        
	    // 如果文件不存在,创建文件. 
	    File file=new File(filenameTemp); 
	    try { 
	      // 若文件存在,先删除已经存在的文件. 如果不想每次手动删除文件,则取消这一块注释
	      //if(file.exist()){
	      //  file.delete();
	      //}
	      // 若文件不存在, 创建文件. 
	      if (!file.exists()) { 
	        file.getParentFile().mkdirs(); 
	        file.createNewFile();  
	      }
	    } catch (IOException e) { 
	      e.printStackTrace(); 
	    }
	        
	    // 向指定文件中写入文字 
	    FileWriter fileWriter; 
	    try {  
	      // 打开一个写文件器，构造函数中的第二个参数true表示以追加形式写文件
	      //fileWriter = new FileWriter(filenameTemp,true); 
	      fileWriter = new FileWriter(filenameTemp); 
	      //使用缓冲区比不使用缓冲区效果更好，因为每趟磁盘操作都比内存操作要花费更多时间。  
	      //通过BufferedWriter和FileWriter的连接，BufferedWriter可以暂存一堆数据，然后到满时再实际写入磁盘  
	      //这样就可以减少对磁盘操作的次数。如果想要强制把缓冲区立即写入,只要调用writer.flush();这个方法就可以要求缓冲区马上把内容写下去  
	      BufferedWriter bufferedWriter=new BufferedWriter(fileWriter);  
	      bufferedWriter.write(filein);  
	      bufferedWriter.close();  
	    } catch (IOException e) {  
	      e.printStackTrace();  
	    }
	  }

}
