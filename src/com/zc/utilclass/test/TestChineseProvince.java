package com.zc.utilclass.test;

import com.zc.utilclass.locationutil.main.ChineseProvinceSimple;
import com.zc.utilclass.locationutil.main.Location;

public class TestChineseProvince {

	public static void main(String[] args) {
		ChineseProvinceSimple cp = new ChineseProvinceSimple();
		String p = cp.getProvinceShort("北京");
		System.out.println(p);
		p = cp.getProvinceShort("上海");
		System.out.println(p);
		//p = cp.getProvinceShort("你好");
		System.out.println(p);
		
		Location co = new Location();
		double[] loc = cp.getProvinceCoor("河南");
		co.setLocation(loc);
		System.out.println(co.toJson());
		
	}

}
