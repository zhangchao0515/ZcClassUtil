package com.zc.utilclass.test;

import java.io.IOException;

import com.zc.utilclass.locationutil.main.LocationUtil;

public class TestLocationUtil {

	public static void main(String[] args) {
		
		LocationUtil lu = new LocationUtil();
		try {
			//北京或北京市
			String[] coordinate = lu.getCoordinate("香港特别行政区");  
			System.out.println(coordinate[0]+"  "+coordinate[1]);//0:经度 1:纬度
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
