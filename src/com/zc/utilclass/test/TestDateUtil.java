package com.zc.utilclass.test;

import java.util.Date;

import com.zc.utilclass.dateutil.main.DateUtil;

public class TestDateUtil {

	public static void main(String[] args) {
		Date date = null;
		String str = "";
		DateUtil du = new DateUtil();
		System.out.println(du.getNow());
		//System.out.println(du.getNowTime()); //
		//System.out.println(du.getNowTimeShort());//
		
		System.out.println("--------string2Date--------");
		str = "2018-9-7";
		System.out.println(du.string2Date(str));
		str = "2018-9-7 12:10:1";
		System.out.println(du.string2Date(str));
		str = "2018年9月7日 12时10分1秒";
		System.out.println(du.string2Date(str));
		
		System.out.println("--------date2String--------");
		date = new Date();
		System.out.println(du.date2String(date));
		System.out.println(du.date2String(date,DateUtil.DF1));
		System.out.println(du.date2String(date,DateUtil.DF2));
		System.out.println(du.date2String(date,DateUtil.DF3));
		System.out.println(du.date2String(date,DateUtil.DF4));
		System.out.println(du.date2String(date,DateUtil.DF5));
		System.out.println(du.date2String(date,DateUtil.DF6));
		System.out.println(du.date2String(date,DateUtil.DF7));
		System.out.println(du.date2String(date,DateUtil.DF8));
		
		//
		System.out.println("--------getStringNow--------");
		System.out.println(du.getStringNow());
		System.out.println(du.getStringNow(DateUtil.DF0));
		
		System.out.println("--------getYear--------");
		System.out.println(du.getYear());
		System.out.println(du.getMonth());
		System.out.println(du.getHour());
		System.out.println(du.getMinute());
		
		System.out.println(du.getTime());
		
		//System.out.println(du.getNowDate());
		//
		System.out.println("--------getTwoHour--------");
		System.out.println(du.getTwoHour("10:50", "10:40"));
		
		System.out.println(du.isLeapYear("2018-08-07"));
		System.out.println(du.isLeapYear("2016-08-07"));

	}

}
