package com.zc.utilclass.test;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import com.zc.utilclass.mysqlutil.main.MysqlUtil;


/**
 * 测试 MysqlBase 类
 * @author zhangchao
 * @date 2018-08-02 15:24
 */
public class TestMysqlUtil {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MysqlUtil mb = new MysqlUtil();
		// Java连接MySQL，这里我本地的数据库名mysqltest，
		mb.mysqlConnectJava("root","0515","mysqltest");
		// 连接数据表，并获取表信息，以及做相应处理; 这里我的表名是employee
		mb.connectTable("employee","select * from employee");
		// 输出数据库
		//String str = (String)mb.processTableData(mb.getRs());
		//System.out.println(str);
		
		// 
		testMysql(mb.getRs());
		
		//关闭表连接
		mb.closedTableConnect();
		// Java关闭数据库连接
		mb.mysqlClosedConnectJava();
	}
	
	/*
	 * @date 2018-08-06 19:49
	 * @author zhangchao
	 * 测试数据库
	 */
	public static void testMysql(ResultSet rs) {
		
		try {
			//  获取表结构
	    	ResultSetMetaData metaData =  rs.getMetaData();  
	    	// 得到行的总数
			int columnCount= metaData.getColumnCount();
			System.out.println(columnCount);
			// 
			int flag = 0;
			while(flag==0 && rs.next() && !"7762".equals(metaData.getColumnName(1))) {
				for(int i = 1; i <= columnCount;i++) {  
					flag = 1;
	            	// 添加键值对，比如说{name:Wp}通过name找到wp
	            	String columnName = metaData.getColumnLabel(i); 
	            	
	            	String value = rs.getString(columnName);
	            	System.out.print(columnName +":"+ value+"\t");
	            } 
				System.out.println();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
