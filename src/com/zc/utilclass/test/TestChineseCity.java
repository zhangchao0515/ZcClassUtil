package com.zc.utilclass.test;

import java.util.Arrays;

import com.zc.utilclass.locationutil.main.ChineseCitySimple;

public class TestChineseCity {

	public static void main(String[] args) {
		//test();
		testLoc();
	}
	
	private static void testLoc() {
		ChineseCitySimple cc = new ChineseCitySimple();
		String city = "博尔塔拉蒙古自治州";
		double[] loc  = new double[2];
		loc = cc.getCityLoc(city);
		System.out.println(Arrays.toString(loc));
	}
	
	@SuppressWarnings("unused")
	private static void test() {
		ChineseCitySimple cc = new ChineseCitySimple();
		
		String city = "博尔塔拉蒙古自治州";
		if(city.length()>=3) {
			System.out.println(city.substring(city.length()-3).equals("自治州"));
			System.out.println(city.substring(city.length()-3));
		}
		
		city = "喀什地区";
		if(city.length()>=2) {
			System.out.println(city.substring(city.length()-2).equals("地区"));
			System.out.println(city.substring(city.length()-2));
		}
		
		city = "神农架林区";
		if(city.length()>=2) {
			System.out.println(city.substring(city.length()-2).equals("地区"));
			System.out.println(city.substring(city.length()-2));
		}
		
		city = "阿拉善盟";
		System.out.println(city.substring(city.length()-1).equals("盟"));
		System.out.println(city.substring(city.length()-1));
		
		//city = "博尔塔拉蒙古自治州";
		//city = "喀什地区";
		//city = "神农架林区";
		//city = "阿拉善盟";
		//city = "乌鲁木齐";
		city = "乌鲁木齐市";
		//city = "香港";
		//city = "";
		//city = null;
		if(city!="" && city!=null && (!city.equals("香港")) && (!city.equals("澳门")) && (!cc.isContainsField(city))) {
			System.out.println(city.substring(city.length()-1));
			if(!city.substring(city.length()-1).equals("市"))	{
				//city.concat("市");
				city = city.concat("市");
			}	
		}
		System.out.println(city);
		if(city!="" && city!=null &&  city.substring(city.length()-1).equals("市")) {
			city = city.substring(0, city.length()-1);
			System.out.println(city);
		}
	}

	private static int count() {
		int countShi = 0;
		int countZi = 0;
		int countDi = 0;
		int countLin = 0;
		int countMeng = 0;
		
		return 0;
	}
}
