package com.zc.utilclass.test;

import com.zc.utilclass.arrayutil.main.ArrayUtil;

public class TestArrayUtil {

	public static void main(String[] args) {
		ArrayUtil m=new ArrayUtil();
		m.add("15");
		m.add("16");
		m.add("17");
		m.add("18");
		m.add("19");
		System.out.println("插入之前：");
		m.print();
		m.insert(2,"22");
		System.out.println("插入之后：");
		m.print();	
		
		//char
		ArrayUtil m2=new ArrayUtil();
		m2.add('d');
		m2.add('m');
		System.out.println("插入之前：");
		m2.print();
		m2.insert(1,"g");
		System.out.println("插入之后：");
		m2.print();	
	}

}
