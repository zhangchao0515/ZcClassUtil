package com.zc.utilclass.stringutil.main;

/**
 * @author zhangchao 
 * 自己封装的字符串（String）工具类
 */
public class StringUtil {

	private String str; // 字符串
	
	public StringUtil(String str) {
		this.setStr(str);
	}
	
	public String getStr() {
		return str;
	}

	public void setStr(String str) {
		this.str = str;
	}

	/**
	 * @param ch 字节
	 * @return 该字节在字符串中出现的次数
	 */
	public long countChar(char ch) {
		long cou = 0;
		for(int i=0; i<str.length(); i++) {
			if(ch == str.charAt(i)) {
				cou++;
			}
		}
		return cou;
	}
	
	/*
	 * 统计s字符串在str中出现的次数
	 */
	public long countString(String s) {
		long cou=0;
		if(s.length()<=this.getStr().length()) {
			int i=0;
			while(i<=(this.getStr().length()-s.length()+1)) {
				int j = this.getStr().indexOf(s,i);
				if(j == i){
					cou++;
					i++;
				}else if(j==-1) {
					i++;
				}else {
					i=j;
				}
			}
		}else {
			System.out.println("输入字符串比匹配字符串长，错误!!"); 
			//System.exit(0);//退出整个程序
			throw new IllegalArgumentException("输入字符串比匹配字符串长，错误!!");
		}
		return cou;
	}
	
	/**
	 * 在字符串特定位置插入字符串
	 * @param loc 在该位置之后插入字符串
	 * @param str 插入的字符串
	 * @return
	 */
	public String insert(int loc, String str) {
		String test = this.getStr();
		StringBuilder sb = new StringBuilder(test);//构造一个StringBuilder对象
        sb.insert(loc, str);//在指定的位置loc，插入指定的字符串str
        test = sb.toString();
        this.setStr(test);
        return test;
	}
}
