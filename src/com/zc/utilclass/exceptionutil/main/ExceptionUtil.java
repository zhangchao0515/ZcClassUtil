package com.zc.utilclass.exceptionutil.main;

/**
 * 
 * @author zhangchao
 * 封装的自己的异常类
 */
@SuppressWarnings("serial")
public class ExceptionUtil extends Exception { //继承自Exception父类
	private String detail;
	
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	
	public ExceptionUtil(String detail) {
		this.setDetail(detail);
    }
    public String toString() {
    	return "ExceptionUtil: " + this.getDetail();
    }
}
