package com.zc.utilclass.exceptionutil.main;

public class TestExceptionUtil {

	public static void main(String[] args) {
		try {
			compute(1); // a小于10，常规退出
			compute(20); // a大于10，则抛出异常
		} catch (ExceptionUtil e) {
			e.printStackTrace();
			//System.out.println("捕捉 " + e); // 这样就可以用自己定义的类来捕捉异常了
		}
	}

	static void compute(int a) throws ExceptionUtil {
		System.out.println("调用 compute(" + a + ")");
		if (a > 10) { // a大于10，则抛出异常
			throw new ExceptionUtil("输入不应该大于10");
			//throw new IllegalArgumentException("输入不应该大于10");
		}
		System.out.println("常规退出 ");
	}

}


