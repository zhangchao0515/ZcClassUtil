package com.zc.utilclass.counterutil.main;

/**
 * @author zhangchao
 * @date 2018-08-23
 * 每次加1
 */
public class CounterUtil2 {
	
	public CounterUtil2() {
	}
	
	/**
	 * @author zhangchao
	 * @param cou 
	 * @return 
	 * 加1
	 */
	public int addition(int cou) {
		return ++cou;
	}
	
	/**
	 * @author zhangchao
	 * @param lo
	 * @return
	 * 加1
	 */
	public long addition(long lo) {
		return ++lo;
	}
	
	/**
	 * @author zhangchao
	 * @param lo  自增的变量
	 * @param step  每次增加step
	 * @return
	 * 每次自增step
	 */
	public long addition(long lo,int step) {
		return lo + step;
	}
	
	/*public boolean checkNumber(String str) {
		if(str.charAt(0) == '0' && str.length() != 0)
			return false;
		for(int i=0; i<str.length(); i++) {
			char ch = str.charAt(i);
			if(!(ch <= '9' && ch >= '0')) 
				return false;
		}
		return true;
	}*/
	
	/**
	 * @author zhangchao
	 * @date 2018-08-23
	 * 每次加1，用String格式（可以表示很大的数）
	 * @param str 数字字符串
	 */
	public String addition(String str) {
		int len = str.length();
		//System.out.println(str.charAt(len-1));
		if(str.charAt(0) == '0' && str.length() != 0) {
			System.out.println("输入数据格式错误！");
		}
			
		int flag = 0;
		char[] charr = str.toCharArray();
		int cou = 0;
		for(int j = len-1; j>=0 && flag==0 ; j--) {
			char ch = str.charAt(j);
			if(ch >= '0' && ch <= '8') { //加1，结束
				ch++;
				charr[j] = ch;
				str = String.valueOf(charr);
				flag = 1;
			}else if(ch=='9') { //变0，进1位
				//char[] charr = str.toCharArray();
				charr[j] = '0';
				str = String.valueOf(charr);
				cou++;
			}else {
				System.out.println("输入数据格式错误！");
			}
		}
		// 超出总位数，在最前面加1
		if(cou == len) {
			str = "1".concat(str);
		}
		//String str1 = "len = " + len + "    cou = " + cou + "    "; 
		return str;
	}
	
	/**
	 * @author zhangchao
	 * @param str
	 * @param step 
	 * @return (str的值 + step的值)
	 * 每次加step的值，用String格式（可以表示很大的数）
	 */
	public String addition(String str,String step) {
		//
		if(str.charAt(0) == '0' && str.length() != 0) {
			System.out.println("输入数据格式错误！");
		}
		if(step.charAt(0) == '0' && step.length() != 0) {
			System.out.println("输入数据格式错误！");
		}
		//如果str长度小于step的，则交换
		if(str.length() < step.length()) { 
			String swap = str;
			str = step;
			step = swap;
		}
		int len = str.length();
		//System.out.println(str.charAt(len-1));
		int flag = 0; //没有进位就为1，退出循环。
		char[] charr = str.toCharArray(); //临时的
		int cou = 0; //计数进位次数
		int temp = 0; //临时的值
		int temp2 = 0; //临时的值
		// a + b + c ，a表示第一个加数的第i位的之，b表示第二个加数的第i位的之，c表示进位；
		//  9+9+9=27 ，因此最大是27 ，两位数
		int carry = 0;  //进位数
		for(int j = len-1,k = step.length()-1; j>=0 && flag==0 ; j--) {
			char ch = str.charAt(j);
			char ch2 = '0';
			if(k >= 0) {
				ch2 = step.charAt(k);
				if(!(ch >= '0' && ch <= '9')) {
					System.out.println("输入数据格式错误！");
				}
				k--;
			}else {
				ch2 = '0';
			}

			if(ch >= '0' && ch <= '9') { //加1，结束
				temp = ch-'0';
				temp2 = ch2-'0';
				temp += (temp2 + carry); // 9+9=18，进位最多为1
				if(temp >= 10) {  //变位
					carry = temp/10; //两个数相加，最高进位只能为0或者1，比如999+99=1098
					temp = temp % 10;
					cou++;
				}else { //不变位
					flag = 1;
				}
				charr[j] = (char) (temp + '0');
				str = String.valueOf(charr);
			}else {
				System.out.println("输入数据格式错误！");
			}
		}
		// 超出总位数，在最前面加1
		if(cou == len) {
			str = "1".concat(str);
		}
		//String str1 = "len = " + len + "    cou = " + cou + "    "; 
		return str;
	}

}
