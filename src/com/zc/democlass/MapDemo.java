package com.zc.democlass;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class MapDemo {

	public static void main(String[] args) {
		testMap1();
		System.out.println();
		testMap2();
		System.out.println();
		testMap3();
		System.out.println();
		testMap4();
	}
	
	//
	public static void testMap1(){
        Map<String,String> map = new HashMap<String,String>();
        map.put("b","1");
        map.put("a","3");
        map.put("c","6");
        map.put("d","8");
        map.put("e","9");
        map.put("f","3");

        //通过map.keySet()方法
        //方法一：通过循环得到key的值，然后通过get（key）获取value;
        for(String key : map.keySet()){
            String value = map.get(key);
            System.out.println(key+"  "+value);
        }
        
        String val = map.get("c");
        System.out.println(val);
    }
	
	public static void testMap2(){
        Map<String,String> map = new HashMap<String,String>();
        map.put("b","1");
        map.put("a","3");
        map.put("c","6");
        map.put("d","8");
        map.put("e","9");
        map.put("f","3");

        //方法二：使用迭代器，获取key;然后判断迭代器是否有下一个。
        Iterator<String> iter = map.keySet().iterator();
        while(iter.hasNext()){
            String key=iter.next();
            String value = map.get(key);
            System.out.println(key+" "+value);
        }
       
        String val = map.get("c");
        System.out.println(val);
    }
	
	public static void testMap3(){
        Map<String,String> map = new HashMap<String,String>();
        map.put("b","1");
        map.put("a","3");
        map.put("c","6");
        map.put("d","8");
        map.put("e","9");
        map.put("f","3");

        //通过map.entrySet()方法
        //方法三：循环map里面的每一对键值对，然后获取key和value
        for(Map.Entry<String, String> vo : map.entrySet()){
            vo.getKey();
            vo.getValue();
            System.out.println(vo.getKey()+"  "+vo.getValue());
        }

        String val = map.get("c");
        System.out.println(val);
    }
	
	public static void testMap4(){
        Map<String,String> map = new HashMap<String,String>();
        map.put("b","1");
        map.put("a","3");
        map.put("c","6");
        map.put("d","8");
        map.put("e","9");
        map.put("f","3");

        //方法四：使用迭代器，获取key
        Iterator<Entry<String,String>> iter = map.entrySet().iterator();
        while(iter.hasNext()){
            Entry<String,String> entry = iter.next();
            String key = entry.getKey();
            String value = entry.getValue();
            System.out.println(key+" "+value);
        }
        
        String val = map.get("c");
        System.out.println(val);
    }

}
