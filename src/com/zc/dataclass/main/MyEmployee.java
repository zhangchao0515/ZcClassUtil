package com.zc.dataclass.main;

import java.util.Date;

public class MyEmployee {

	// 工号
	private int empno;	
	// 名字
	private String ename;
	// 工作
	private String job;
	// 雇佣日期 
	private Date hiredate;
	// 工资
	private float sal;
	
	public MyEmployee() {
		
	}
	
	public int getEmpno() {
		return empno;
	}

	public void setEmpno(int empno) {
		this.empno = empno;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public Date getHiredate() {
		return hiredate;
	}

	public void setHiredate(Date hiredate) {
		this.hiredate = hiredate;
	}

	public float getSal() {
		return sal;
	}

	public void setSal(float sal) {
		this.sal = sal;
	}
	
}
